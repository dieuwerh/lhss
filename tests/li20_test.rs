use lhss::{
    misc::{num_traits::AugmentFile, util::basic_test_setup},
    schemes::li20::{Li2020, Sk, Tag},
};
use rand_core::{RngCore, SeedableRng};

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    Vec<Vec<bls12_381::Scalar>>,
    Li2020,
    &'static [u8; 17],
    lhss::schemes::li20::Sk,
    &'static [u8; 15],
    Tag,
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let file = file.augment(false);
    let scheme = Li2020::setup(&mut rng);
    let signer_id = b"thisisthesignerid";
    let key = scheme.keyext(signer_id, &mut rng);
    let file_id = b"thisisthefileid";
    let tag = Tag::new(file_id, &key.big_r);
    (fl, pl, rng, file, scheme, signer_id, key, file_id, tag)
}

#[test]
fn sign_packet() {
    let (_fl, pl, _rng, file, scheme, signer_id, key, file_id, tag) = setup();
    let packet = &file[0];
    let sig = scheme.sign(&key, signer_id, file_id, packet, pl);

    let check = scheme.verify(signer_id, &tag, pl, packet, &sig);
    assert!(check);
}

#[test]
fn sign_file_single_signer() {
    let (fl, pl, _rng, file, scheme, signer_id, key, file_id, tag) = setup();
    let sigs = scheme.sign_file_single_key(&key, signer_id, file_id, &file, pl);
    let chekcs = scheme.verify_file_single_vk(signer_id, &tag, &file, &sigs, pl);
    assert_eq!(chekcs, vec![true; fl]);
}

#[test]
fn sign_file_multiple_signers() {
    let (fl, pl, mut rng, file, scheme, _signer_id, _key, file_id, _tag) = setup();
    let mut signer_ids = vec![[0u8; 32]; fl];
    for signer_id in &mut signer_ids {
        rand::thread_rng().fill_bytes(signer_id);
    }
    let signer_ids: Vec<Vec<u8>> = signer_ids.iter().map(|id| id.to_vec()).collect();

    let keys: Vec<Sk> = signer_ids
        .iter()
        .map(|id| scheme.keyext(id, &mut rng))
        .collect();
    // for (key, id) in keys.iter().zip(&signer_ids) {
    //     println!("Key checks out: {:#}", scheme.verify_private_key(id, key));
    // }
    let sigs = scheme.sign_file_multiple_keys(&keys, &signer_ids, file_id, &file, pl);
    let tags: Vec<Tag> = keys.iter().map(|k| Tag::new(file_id, &k.big_r)).collect();

    let checks = scheme.verify_file_multiple_vks(&signer_ids, &tags, &file, &sigs, pl);

    assert_eq!(checks, vec![true; fl]);
}

#[test]
fn test_key_gen() {
    let mut rng = rand_chacha::ChaCha8Rng::seed_from_u64(2);
    let (_fl, _pl, mut _rng, _file, scheme, _signer_id, _key, _file_id, _tag) = setup();
    let signer_id = b"this is the signer id";
    let id_in_vec = vec![signer_id];
    let key1 = scheme.keyext(signer_id, &mut rng);
    let mut rng = rand_chacha::ChaCha8Rng::seed_from_u64(2);
    let key2 = scheme.keyext(&*id_in_vec[0], &mut rng);

    assert_eq!(key1, key2)
}
