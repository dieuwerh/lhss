use std::{collections::HashMap, vec};

use bls12_381::Scalar;
use lhss::{
    misc::util::{basic_test_setup, sum_file},
    schemes::schabhuser18::{Key, Label, MLProg, Schab2018},
};
use rand_core::RngCore;

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    Vec<Vec<bls12_381::Scalar>>,
    Schab2018,
    &'static [u8; 22],
    lhss::schemes::schabhuser18::Key,
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let max_identities = fl;
    let scheme = Schab2018::setup(max_identities, fl, pl, &mut rng);
    let did = b"this is the dataset id";
    let key = scheme.keygen(&mut rng);
    (fl, pl, rng, file, scheme, did, key)
}

#[test]
/// Test signing a packet and verifying it
pub fn sign_packet() {
    let (_fl, _pl, mut rng, file, scheme, did, key) = setup();
    let packet = &file[0];
    let label = Label::new(0, 0);
    let sig = scheme.sign(&key.sk, did, &label, packet, &mut rng);
    let function = vec![Scalar::one()];
    let mut keys = HashMap::new();
    keys.insert(label.id, key.pk);
    let prog = MLProg::new(did, vec![label], function);
    let check = scheme.verify(&prog, &keys, packet, &sig);
    assert!(check)
}

#[test]
/// Test singing each packet in a file using the same signing key.
/// After that, verify each individual signature with the same verifying key
pub fn sign_file_single_signer() {
    let (fl, _pl, mut rng, file, scheme, did, key) = setup();
    let labels = (0..fl).map(|i| Label::new(i, 0)).collect();
    let sigs = scheme.sign_file_single_key(&key.sk, did, &labels, &file, &mut rng);
    let mut vks = HashMap::new();
    vks.insert(labels[0].id, key.pk);
    let function = vec![Scalar::one()];
    let mut programs = Vec::new();
    for label in labels {
        let prog = MLProg::new(did, vec![label], function.clone());
        programs.push(prog);
    }

    let checks = scheme.verify_file(&vks, &programs, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
/// Test signing each packet in a file using a different signing key.
/// After that, verify each individual signature with the corresponding verification key.
pub fn sign_file_multiple_signers() {
    let (fl, _pl, mut rng, file, scheme, did, _key) = setup();
    let labels: Vec<Label> = (0..fl).map(|i| Label::new(i, i as u8)).collect();
    let keys: Vec<Key> = (0..fl).map(|_| scheme.keygen(&mut rng)).collect();
    let sks = keys.iter().map(|k| k.sk.clone()).collect();
    let sigs = scheme.sign_file_multiple_keys(&sks, did, &labels, &file, &mut rng);
    let mut vks = HashMap::new();
    for (label, key) in labels.iter().zip(keys) {
        vks.insert(label.id, key.pk);
    }
    let function = vec![Scalar::one()];
    let mut programs = Vec::new();
    for label in labels {
        let prog = MLProg::new(did, vec![label], function.clone());
        programs.push(prog);
    }

    let checks = scheme.verify_file(&vks, &programs, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
/// Test signing each packet in a file using a different key.
/// After that, combine the packets by summing them, and combine the signatures correspondingly.
/// Finally, verify the combined signature and packet.
pub fn combine_sum_file_multiple_signers() {
    let (fl, _pl, mut rng, file, scheme, did, _key) = setup();
    let labels: Vec<Label> = (0..fl).map(|i| Label::new(i, i as u8)).collect();
    let keys: Vec<Key> = (0..fl).map(|_| scheme.keygen(&mut rng)).collect();
    let sks = keys.iter().map(|k| k.sk.clone()).collect();
    let sigs = scheme.sign_file_multiple_keys(&sks, did, &labels, &file, &mut rng);
    let mut vks = HashMap::new();
    for (label, key) in labels.iter().zip(keys) {
        vks.insert(label.id, key.pk);
    }
    let sum_function = vec![Scalar::one(); fl];
    let combined_signature = scheme.combine(&sum_function, &sigs);
    let combined_packet = sum_file(&file);
    let program = MLProg::new(did, labels, sum_function);
    let check = scheme.verify(&program, &vks, &combined_packet, &combined_signature);
    assert!(check)
}
