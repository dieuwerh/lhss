use bls12_381::Scalar;
use lhss::{
    misc::{
        num_traits::AugmentFile,
        util::{basic_test_setup, combine_file},
    },
    schemes::li18::{Key, Li18},
};
use rand_core::RngCore;

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    Vec<Vec<Scalar>>,
    Scalar,
    Li18,
    lhss::schemes::li18::Key,
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let file = file.augment(true);
    let scheme = Li18::setup(pl, fl, &mut rng);
    let key = scheme.keygen(&mut rng);
    let file_id = Scalar::from(1234);
    (fl, pl, rng, file, file_id, scheme, key)
}

#[test]
fn test_sign_packet() {
    let (_fl, _pl, mut rng, file, file_id, scheme, key) = setup();
    let packet = &file[0];
    let sig = scheme.sign(&key.sk, packet, &file_id, 0usize, &mut rng);
    let check = scheme.verify(packet, &sig, &file_id, &vec![key.vk]);
    assert!(check)
}

#[test]
fn test_sign_file() {
    let (fl, _pl, mut rng, file, file_id, scheme, key) = setup();
    let sigs = scheme.sign_file_single_key(&key.sk, &file, &file_id, &mut rng);
    let checks = scheme.verify_file_single_vk(&file, &sigs, &file_id, &key.vk);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
fn test_sign_file_multiple_signers() {
    let (fl, _pl, mut rng, file, file_id, scheme, _key) = setup();
    let keys = (0..fl).map(|_| scheme.keygen(&mut rng)).collect();
    let sigs = scheme.sign_file_multiple_keys(&keys, &file, &file_id, &mut rng);
    let vks = &keys.iter().map(|k| k.vk).collect();
    let checks = scheme.verify_file_multiple_vks(&file, &sigs, &file_id, vks);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
fn test_sum_file_multiple_signers() {
    let (fl, _pl, mut rng, file, file_id, scheme, _key) = setup();
    let keys: Vec<Key> = (0..fl).map(|_| scheme.keygen(&mut rng)).collect();
    let vks = keys.iter().map(|k| k.vk.clone()).collect();
    let sigs = scheme.sign_file_multiple_keys(&keys, &file, &file_id, &mut rng);
    let coefs = vec![Scalar::one(); fl];
    let combined_file = combine_file(&file, &coefs);
    let combined_sig = scheme.combine(&coefs, &sigs);
    let check = scheme.verify(&combined_file, &combined_sig, &file_id, &vks);
    assert!(check)
}
