use lhss::{misc::util::basic_test_setup, schemes::zhang18::NCZha2018};

use rand::thread_rng;
use rand_core::RngCore;

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    Vec<Vec<bls12_381::Scalar>>,
    NCZha2018,
    &'static [u8; 21],
    lhss::schemes::zhang18::SK,
    &'static [u8; 19],
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let scheme = NCZha2018::setup(pl, fl, &mut rng);
    let signer_id = b"this is the signer id";
    let key = scheme.extract(&signer_id.to_vec(), &mut rng);
    let file_id = b"this is the file id";
    (fl, pl, rng, file, scheme, signer_id, key, file_id)
}

#[test]
pub fn sign_packet() {
    let (_fl, _pl, _rng, file, scheme, signer_id, key, file_id) = setup();
    let packet = &file[0];
    let sig = scheme.sign(&key, file_id, packet);
    let check = scheme.verify(&signer_id.to_vec(), &key.hr, file_id, packet, &sig, &key.gs);
    assert!(check);
}

#[test]
pub fn sign_file_single_signer() {
    let (fl, _pl, _rng, file, scheme, signer_id, key, file_id) = setup();
    let sigs = scheme.sign_file_single_key(&key, file_id, &file);
    let checks =
        scheme.verify_file_single_vk(&signer_id.to_vec(), file_id, &key.hr, &key.gs, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
pub fn sign_file_multiple_signers() {
    let (fl, _pl, mut rng, file, scheme, _signer_id, _key, file_id) = setup();
    let ids: Vec<Vec<u8>> = (0..fl)
        .map(|_| {
            let mut id = [0u8; 8];
            thread_rng().fill_bytes(&mut id);
            id.to_vec()
        })
        .collect();
    let keys = ids.iter().map(|id| scheme.extract(id, &mut rng)).collect();
    let sigs = scheme.sign_file_multiple_keys(&keys, file_id, &file);
    let key_hrs = keys.iter().map(|k| k.hr.clone()).collect();
    let key_gs = keys.iter().map(|k| k.gs.clone()).collect();
    let checks = scheme.verify_file_multiple_vks(&ids, file_id, &key_hrs, &key_gs, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}
