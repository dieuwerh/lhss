use lhss::{
    misc::{num_traits::AugmentFile, util::basic_test_setup},
    schemes::lin17::Lin17,
};
use rand_core::RngCore;

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    Vec<Vec<bls12_381::Scalar>>,
    Lin17,
    &'static [u8; 15],
    lhss::schemes::lin17::Key,
    lhss::schemes::lin17::Key,
    lhss::schemes::lin17::Key,
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let file = file.augment(false);
    let scheme = Lin17::setup(pl, fl, &mut rng);
    let file_id = b"thisisthefileid";
    let key_a = scheme.keygen(&mut rng);
    let key_b = scheme.keygen(&mut rng);
    let key_c = scheme.keygen(&mut rng);
    (fl, pl, rng, file, scheme, file_id, key_a, key_b, key_c)
}

#[test]
pub fn test_sign_packet() {
    let (_fl, _pl, _rng, file, scheme, file_id, key_a, key_b, _key_c) = setup();
    let packet = &file[0];
    let sig = scheme.sign(&key_a.sk, &key_b.pk, file_id, packet);
    let check = scheme.verify(&key_a.pk, &key_b.sk, file_id, &sig, packet);
    assert!(check);
}

#[test]
pub fn sign_file_single_signer() {
    let (fl, _pl, _rng, file, scheme, file_id, key_a, key_b, _key_c) = setup();
    let sigs = scheme.sign_file_single_key(&key_a.sk, &key_b.pk, file_id, &file);
    let checks = scheme.verify_file_single_vk(&key_a.pk, &key_b.sk, file_id, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}
