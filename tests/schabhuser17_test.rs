use bls12_381::Scalar;
use lhss::{
    misc::util::basic_test_setup,
    schemes::schabhuser17::{Key, MLProg, Sch17},
};
use rand_core::RngCore;

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    Vec<Vec<bls12_381::Scalar>>,
    Sch17,
    &'static [u8; 22],
    lhss::schemes::schabhuser17::Key,
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let scheme = Sch17::setup(fl, pl, &mut rng);
    let did = b"this is the dataset id";
    let key = scheme.keygen(&mut rng);
    (fl, pl, rng, file, scheme, did, key)
}

#[test]
pub fn sign_packet() {
    let (fl, _pl, _rng, file, scheme, did, key) = setup();
    let packet = &file[0];
    let sig = scheme.sign(&key.sk, did, 0, packet);
    let mut function = vec![Scalar::zero(); fl];
    function[0] = Scalar::one();

    let program = MLProg::new(function, did.to_vec());
    let check = scheme.verify(&key.vk, &program, packet, &sig);
    assert!(check);
}

#[test]
pub fn sign_file_single_signer() {
    let (fl, _pl, _rng, file, scheme, did, key) = setup();
    let sigs = scheme.sign_file_single_key(&key.sk, did, &file);

    let function = vec![Scalar::zero(); fl];
    let progs = (0..fl)
        .map(|i| {
            let mut res = function.clone();
            res[i] = Scalar::one();
            MLProg::new(res, did.to_vec())
        })
        .collect();

    let checks = scheme.verify_file_single_vk(&key.vk, &progs, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
pub fn sign_file_multiple_keys() {
    let (fl, _pl, mut rng, file, scheme, did, _key) = setup();
    let keys: Vec<Key> = (0..fl).map(|_| scheme.keygen(&mut rng)).collect();
    let sks = keys.iter().map(|k| k.sk.clone()).collect();
    let vks = keys.iter().map(|k| k.vk.clone()).collect();
    let sigs = scheme.sign_file_multiple_keys(&sks, did, &file);

    let function = vec![Scalar::zero(); fl];
    let progs = (0..fl)
        .map(|i| {
            let mut res = function.clone();
            res[i] = Scalar::one();
            MLProg::new(res, did.to_vec())
        })
        .collect();

    let checks = scheme.verify_file_multiple_vks(&vks, &progs, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}
