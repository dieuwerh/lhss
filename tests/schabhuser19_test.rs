use std::collections::HashMap;

use bls12_381::Scalar;
use lhss::{
    misc::util::{basic_test_setup, sum_file},
    schemes::schabhuser19::{Key, Label, MLProg, Sch2019},
};
use rand_core::RngCore;

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    Vec<Vec<bls12_381::Scalar>>,
    Sch2019,
    &'static [u8; 22],
    lhss::schemes::schabhuser19::Key,
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let max_identities = fl;
    let scheme = Sch2019::setup(max_identities, fl, pl, &mut rng);
    let did = b"this is the dataset id";
    let key = scheme.keygen(&mut rng);
    (fl, pl, rng, file, scheme, did, key)
}

#[test]
pub fn sign_packet() {
    let (_fl, _pl, mut rng, file, scheme, did, key) = setup();
    let packet = &file[0];
    let id = vec![123];
    let label = Label::new(id, 0);
    let sig = scheme.sign(&key.sk, did, &label, packet, &mut rng);
    let mut id_pk_map = HashMap::new();
    id_pk_map.insert(label.id.clone(), key.pk);
    let prog = MLProg::new(&did.to_vec(), &vec![label], &vec![Scalar::one()]);
    let check = scheme.verify(&prog, &id_pk_map, packet, &sig);
    assert!(check);
}

#[test]
pub fn sign_file_single_signer() {
    let (fl, _pl, mut rng, file, scheme, did, key) = setup();
    let id = vec![123];
    let mut labels = vec![Label::new(id, 0); fl];
    for (i, label) in labels.iter_mut().enumerate() {
        label.tag = i;
    }
    let sigs = scheme.sign_file_single_key(&key.sk, did, &labels, &file, &mut rng);
    let progs = labels
        .iter()
        .map(|label| MLProg::new(&did.to_vec(), &vec![label.clone()], &vec![Scalar::one()]))
        .collect();
    let mut id_pk_map = HashMap::new();
    id_pk_map.insert(labels[0].id.clone(), key.pk);
    let checks = scheme.verify_file(&id_pk_map, &progs, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
pub fn sign_file_multiple_signers() {
    let (fl, _pl, mut rng, file, scheme, did, _key) = setup();
    let mut labels = Vec::new();
    for i in 0..fl {
        labels.push(Label {
            id: vec![i as u8; 8],
            tag: i,
        });
    }
    let keys = (0..fl)
        .map(|_| scheme.keygen(&mut rng))
        .collect::<Vec<Key>>();
    let sks = keys.iter().map(|k| k.sk.clone()).collect();

    let sigs = scheme.sign_file_multiple_keys(&sks, &did.to_vec(), &labels, &file, &mut rng);
    let progs = labels
        .iter()
        .map(|label| MLProg::new(&did.to_vec(), &vec![label.clone()], &vec![Scalar::one()]))
        .collect();
    let mut id_pk_map = HashMap::new();
    for (label, key) in labels.iter().zip(keys) {
        id_pk_map.insert(label.id.clone(), key.pk);
    }

    let checks = scheme.verify_file(&id_pk_map, &progs, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
pub fn combine_sum_file_multiple_signers() {
    let (fl, _pl, mut rng, file, scheme, did, _key) = setup();
    let mut labels = Vec::new();
    for i in 0..fl {
        labels.push(Label {
            id: vec![i as u8; 8],
            tag: i,
        });
    }
    let keys = (0..fl)
        .map(|_| scheme.keygen(&mut rng))
        .collect::<Vec<Key>>();
    let sks = keys.iter().map(|k| k.sk.clone()).collect();

    let sigs = scheme.sign_file_multiple_keys(&sks, &did.to_vec(), &labels, &file, &mut rng);
    let mut id_pk_map = HashMap::new();
    for (label, key) in labels.iter().zip(keys) {
        id_pk_map.insert(label.id.clone(), key.pk);
    }
    let sum_function = vec![Scalar::one(); fl];
    let summed_signature = scheme.combine(&sum_function, &sigs);
    let summed_packet = sum_file(&file);
    let sum_program = MLProg::new(&did.to_vec(), &labels, &sum_function);
    let check = scheme.verify(&sum_program, &id_pk_map, &summed_packet, &summed_signature);
    assert!(check)
}
