use bls12_381::Scalar;
use lhss::{
    self,
    misc::{types::File, util::basic_test_setup},
    schemes::aranha19::{Ara2019, Key, Label, LabeledProgram, Pk, Program},
};
use rand_core::RngCore;

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    File<Scalar>,
    Ara2019,
    Key,
    Vec<Label>,
    Vec<LabeledProgram>,
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let scheme = Ara2019::setup();
    let key = scheme.keygen(&mut rng);
    let id = 123;
    let labels: Vec<Label> = (0..pl).map(|i| Label::new(&id, &(i as u8))).collect();
    let identity_program: Program = vec![Scalar::from(1)];
    let labeled_identity_programs: Vec<LabeledProgram> = (0..pl)
        .map(|i| LabeledProgram::new(&identity_program, &vec![labels[i].clone()]))
        .collect();
    (
        fl,
        pl,
        rng,
        file,
        scheme,
        key,
        labels,
        labeled_identity_programs,
    )
}

#[test]
fn test_sign_packet_single_key() {
    let (_, pl, _, file, scheme, key, labels, labeled_identity_programs) = setup();
    let packet = &file[0];
    let signatures = scheme.sign_packet(&key, &labels, packet);
    let checks =
        scheme.verify_packet_single_vk(&labeled_identity_programs, &key.pk, packet, &signatures);
    assert_eq!(checks, vec![true; pl]);
}

#[test]
fn test_sign_packet_multi_key() {
    let (_, pl, mut rng, file, scheme, _, labels, labeled_identity_programs) = setup();
    let packet = &file[0];
    let keys: Vec<Key> = (0..pl).map(|_| scheme.keygen(&mut rng)).collect();
    let signatures = scheme.sign_packet_multiple_signers(&keys, &labels, packet);

    let checks = scheme.verify_packet(
        &labeled_identity_programs,
        &keys.iter().map(|k| k.pk).collect(),
        packet,
        &signatures,
    );
    assert_eq!(checks, vec![true; pl]);
}

#[test]
fn test_sign_file_single_key() {
    let (fl, pl, _, file, scheme, key, labels, labeled_identity_programs) = setup();
    let signatures = scheme.sign_file_single_signer(&key, &labels, &file);
    let checks =
        scheme.verify_file_single_vk(&labeled_identity_programs, &key.pk, &file, &signatures);
    assert_eq!(checks, vec![vec![true; pl]; fl])
}

#[test]
fn test_sign_file_multiple_signers() {
    let (fl, pl, mut rng, file, scheme, _, labels, labeled_identity_programs) = setup();
    let keys: Vec<Vec<Key>> = (0..fl)
        .map(|_| (0..pl).map(|_| scheme.keygen(&mut rng)).collect())
        .collect();
    let signatures = scheme.sign_file_multiple_signers(&keys, &labels, &file);
    let vks: Vec<Vec<Pk>> = keys
        .iter()
        .map(|packet_keys| packet_keys.iter().map(|k| k.pk).collect())
        .collect();
    let checks = scheme.verify_file(&labeled_identity_programs, &vks, &file, &signatures);
    assert_eq!(checks, vec![vec![true; pl]; fl])
}
