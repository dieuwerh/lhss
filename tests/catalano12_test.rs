use bls12_381::Scalar;
use lhss::{
    misc::{num_traits::AugmentFile, types::File, util::basic_test_setup},
    schemes::catalano12::{Cat12, Key},
};
use rand_core::RngCore;

fn setup() -> (usize, usize, impl RngCore, File<Scalar>, Scalar, Cat12, Key) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let file = file.augment(true);
    let scheme = Cat12::setup(pl, fl);
    let key = scheme.keygen(&mut rng);
    let file_id = Scalar::from(1234);
    (fl, pl, rng, file, file_id, scheme, key)
}

#[test]
fn test_sign_packet() {
    let (_, _, mut rng, file, file_id, scheme, key) = setup();
    let packet = &file[0];
    let sig = scheme.sign(&key, &file_id, packet, &mut rng);
    let check = scheme.verify(&key.vk, &file_id, packet, &sig);
    assert!(check)
}

#[test]
fn test_sign_file() {
    let (fl, _, mut rng, file, file_id, scheme, key) = setup();
    let sigs = scheme.sign_file_single_key(&key, &file_id, &file, &mut rng);
    let checks = scheme.verify_file_single_vk(&key.vk, &file_id, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
fn test_sign_file_multiple_signers() {
    let (fl, _, mut rng, file, file_id, scheme, _) = setup();
    let keys = (0..fl).map(|_| scheme.keygen(&mut rng)).collect();
    let sigs = scheme.sign_file_multiple_keys(&keys, &file_id, &file, &mut rng);
    let vks = keys.iter().map(|k| k.vk.clone()).collect();
    let checks = scheme.verify_file_multiple_vks(&vks, &file_id, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}
