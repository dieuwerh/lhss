use bls12_381::Scalar;
use lhss::{
    misc::{types::File, util::basic_test_setup},
    schemes::boneh09::{Bon09, Key},
};
use rand_core::RngCore;

fn setup() -> (
    usize,
    usize,
    impl RngCore,
    File<Scalar>,
    Vec<u8>,
    Bon09,
    Key,
) {
    let (fl, pl, mut rng, file) = basic_test_setup();
    let scheme = Bon09::setup(pl, &mut rng);
    let key = scheme.keygen(&mut rng);
    let file_id = b"boneh test file id";
    (fl, pl, rng, file, file_id.to_vec(), scheme, key)
}

#[test]
fn test_sign_packet() {
    let (_, _, _, file, file_id, scheme, key) = setup();
    let packet = &file[0];
    let signature = scheme.sign(&key.sk, &file_id, packet);
    let check = scheme.verify(&key.pk, &file_id, packet, &signature);
    assert!(check)
}

#[test]
fn test_sign_file() {
    let (fl, _, _, file, file_id, scheme, key) = setup();
    let sigs = scheme.sign_file_single_key(&key.sk, &file_id, &file);
    let checks = scheme.verify_file_single_vk(&key.pk, &file_id, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}

#[test]
fn test_sign_file_multiple_signers() {
    let (fl, _, mut rng, file, file_id, scheme, _) = setup();
    let keys: Vec<Key> = (0..fl).map(|_| scheme.keygen(&mut rng)).collect();
    let sks = keys.iter().map(|k| k.sk).collect();
    let vks = keys.iter().map(|k| k.pk).collect();
    let sigs = scheme.sign_file_multiple_keys(&sks, &file_id, &file);
    let checks = scheme.verify_file(&vks, &file_id, &file, &sigs);
    assert_eq!(checks, vec![true; fl]);
}
