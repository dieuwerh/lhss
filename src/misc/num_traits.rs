/// Adds one vector to the other, combining element wise
pub trait AddVecs {
    fn add_vec(&self, rhs: &Self) -> Self;
    fn add_vec_mut(&mut self, rhs: &Self);
}

/// Scales all elements in a vector by multiplying with the same scale factor
pub trait ScaleVec<S> {
    fn scale(&self, factor: &S) -> Self;
    fn scale_mut(&mut self, factor: &S);
}

pub trait NumsToVector<VecType> {
    fn nums_to_vector(&self) -> VecType;
}
pub trait AugmentFile {
    fn augment(&self, in_front: bool) -> Self;
}

pub trait Ops<T, M> {
    fn add(&self, other: &T) -> T;
    fn sub(&self, other: &T) -> T;
    fn mul(&self, other: &T) -> T;
    fn divscale(&self, fac: &M) -> T;
    fn scale(&self, scale_factor: &M) -> T;
}

pub trait ModOps<T, M> {
    fn addmod(&self, other: &T, modulus: &M) -> T;
    fn mulmod(&self, other: &T, modulus: &M) -> T;
    fn scalemod(&self, scale: &M, modulus: &M) -> T;
}
