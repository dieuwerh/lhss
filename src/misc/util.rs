use bls12_381::Scalar;
use ff::Field;
use rand_core::{RngCore, SeedableRng};

use super::{
    num_traits::{AddVecs, ScaleVec},
    types::{File, Packet},
};

/// Function to generate a random Packet consisting of packet_length Scalar values.
/// Calls Scalar::random packet_length times.
///
/// # Arguments
///
/// * `packet_length` - usize indicatnig how long the packet should be.
/// * `rng` - Implementor of rng core, to generate random scalar values with.
///
/// # Returns
/// * A randomly generated Packet\<Scalar\>
///
pub fn random_scalar_packet(packet_length: usize, rng: &mut impl RngCore) -> Packet<Scalar> {
    let mut res = vec![Scalar::zero(); packet_length];
    for i in 0..packet_length {
        res[i] = Scalar::random(&mut *rng);
    }
    res
}

/// Function to generate a random File consisting of file_lenght packets.
/// The packets consist of packet_length scalar values.
///
/// # Arguments
///
/// * `file_length` - usize indicating how long the file should be
/// * `packet_length` - usize indicating how long the packet should be
/// * `rng` - Implementor of rng core, to generate the random scalars with.
///
/// # Returns
///
/// * A randomly generated File\<Scalar\>
pub fn random_scalar_file(
    file_length: usize,
    packet_length: usize,
    rng: &mut impl RngCore,
) -> File<Scalar> {
    (0..file_length)
        .map(|_| random_scalar_packet(packet_length, rng))
        .collect()
}

/// Function to setup and generate the basics of running a test.
///
/// Sets the file length to 4, the packet lenght to 2
/// Generates a ChaChaRng from entropy
/// Finally generates a Scalar File
///
pub fn basic_test_setup() -> (usize, usize, impl RngCore, File<Scalar>) {
    let file_length = 4;
    let packet_length = 2;
    let mut rng = rand_chacha::ChaChaRng::from_entropy();
    let file = random_scalar_file(file_length, packet_length, &mut rng);
    (file_length, packet_length, rng, file)
}

pub fn combine_file(file: &File<Scalar>, coefs: &Vec<Scalar>) -> Packet<Scalar> {
    let mut res = file[0].clone();
    res.scale(&coefs[0]);

    for i in 1..file.len() {
        let cur = file[i].scale(&coefs[i]);
        res.add_vec_mut(&cur);
    }

    res
}

pub fn sum_file(file: &File<Scalar>) -> Packet<Scalar> {
    let mut res = file[0].clone();
    for i in 1..file.len() {
        res.add_vec_mut(&file[i]);
    }
    res
}
