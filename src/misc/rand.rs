use hmac::{Hmac, Mac};
use num_bigint_dig::{prime::probably_prime, BigUint, RandBigInt, RandPrime};
use num_traits::{One, Zero};
use rand::RngCore;
use sha2_hmac::Sha512;

type HmacSha512 = Hmac<Sha512>;

/// PRF returning 64 bytes
///
/// Uses HMac Sha512 under the hood
/// # Arguments
///
/// * `key` - The key bytes for the prf
/// * `msg` - The msg bytes that get MAC'ed / PRF'ed
pub fn prf(key: &[u8], msg: &[u8]) -> [u8; 64] {
    let mut mac = HmacSha512::new_from_slice(&key).expect("something has gone wrong");
    mac.update(msg);
    let mut output = [0u8; 64];
    output.copy_from_slice(&mac.finalize().into_bytes());
    output
}
        
/// Trait to generate a vector of random elements of T
/// 
pub trait GenRandomVec<T>: RngCore {
    /// ## Arguments:
    /// * `n`: Number of items to return
    fn gen_rand_vec(&mut self, n: usize) -> Vec<T>;
}

/// Trait to generate a save prime number
///
/// Save prime: p is prime and (p-1) / 2 is prime as well
///
/// ## Arguments:
/// * `self` - An implementor of RngCore
/// * `bits` - the bit length of the prime
pub trait GenSavePrime: RngCore {
    fn gen_save_prime(&mut self, bits: usize) -> BigUint;
}

/// Generates a prime number.
/// Checks if it is a save prime, and if not
/// tries a again with a new random prime
impl<R: RngCore> GenSavePrime for R {
    fn gen_save_prime(&mut self, bits: usize) -> BigUint {
        let mut res = self.gen_prime(bits);
        while !is_save_prime(&res) {
            res = self.gen_prime(bits);
        }
        res
    }
}
/// Checks if a number is a save prime
///
/// Save if (num - 1) / 2 is a prime
/// Runs the `probably_prime` function of `num_bigint_dig`,
/// which internally uses the miller-rabbin test and the baillie-pws test
pub fn is_save_prime(num: &BigUint) -> bool {
    let to_check = (num - 1u8) / 2u8;
    probably_prime(&to_check, 20)
}
/// Generate an item of the Quadratic residue of N=p*q
pub trait GenQR: RngCore {
    fn gen_qr(&mut self, p: &BigUint, q: &BigUint) -> BigUint;
}
impl<R: RngCore> GenQR for R {
    fn gen_qr(&mut self, p: &BigUint, q: &BigUint) -> BigUint {
        let n = p * q;
        let mut res = self.gen_biguint_below(&n);
        while !is_qr(&res, p, q) {
            res = self.gen_biguint_below(&n);
        }
        res
    }
}
pub fn is_qr(num: &BigUint, p: &BigUint, q: &BigUint) -> bool {
    let mod_p = num % p;
    let zero = BigUint::zero();
    let one = BigUint::one();
    if mod_p == zero || mod_p == one || mod_p == (p - 1u8) {
        return false;
    }
    let mod_q = num % q;
    if mod_q == zero || mod_q == one || mod_q == (q - 1u8) {
        return false;
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;
    use sha2_hmac::Sha256;
    #[test]
    fn test_sha256_hmac_output_length() {
        let key = b"this is the input key";
        let msg = b"this is the message we are going to mac";
        let mut mac = Hmac::<Sha256>::new_from_slice(&key[..]).expect("something has gone wrong");
        mac.update(msg);
        let result = mac.finalize();
        assert_eq!(result.into_bytes().len(), 32)
    }
    #[test]
    fn test_sha512_hmac_output_length() {
        let key = b"this is the input key";
        let msg = b"this is the message we are going to mac";
        let mut mac = Hmac::<Sha512>::new_from_slice(&key[..]).expect("something has gone wrong");
        mac.update(msg);
        let result = mac.finalize();
        assert_eq!(result.into_bytes().len(), 64)
    }
}
