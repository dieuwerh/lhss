use crate::misc::hashing::hash_to_64_bytes;
use crate::misc::num_traits::{AddVecs, ScaleVec};
use crate::misc::rand;
use bls12_381::hash_to_curve::{ExpandMsgXmd, HashToCurve};
use bls12_381::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Gt, Scalar};
use group::Group;
use ::rand::RngCore;

use super::num_traits::{AugmentFile, NumsToVector};
use super::rand::GenRandomVec;

/// Trait to hash something to G1Projective
///
/// Uses function that implements
/// [draft-irtf-cfrg-hash-to-curve-11](https://datatracker.ietf.org/doc/html/draft-irtf-cfrg-hash-to-curve-11)
pub trait HashesToG1 {
    fn hash_g1(&self) -> G1Projective;
}

impl HashesToG1 for Gt {
    /// Hashes an element of Gt to G1Projective
    ///
    /// First turns the Gt element to a string
    /// Then turns that string into bytes
    /// Finally passes those bytes to hash function
    fn hash_g1(&self) -> G1Projective {
        let string = self.to_string();
        let bytes = string.as_bytes();
        bytes.hash_g1()
    }
}

impl HashesToG1 for &[u8] {
    /// Hashes a slice of bytes to G1Projective
    ///
    /// Uses Sha256 as hashing algorithm
    fn hash_g1(&self) -> G1Projective {
        let dst: &[u8] = b"testDST";
        let hashed =
            <G1Projective as HashToCurve<ExpandMsgXmd<sha2::Sha256>>>::encode_to_curve(self, dst);
        hashed
    }
}

impl HashesToG1 for Vec<u8> {
    /// Hashes a slice of bytes to G1Projective
    ///
    /// Uses Sha256 as hashing algorithm
    fn hash_g1(&self) -> G1Projective {
        let dst: &[u8] = b"testDST";
        let hashed =
            <G1Projective as HashToCurve<ExpandMsgXmd<sha2::Sha256>>>::encode_to_curve(self, dst);
        hashed
    }
}

/// Trait to hash something to a Scalar
pub trait HashesToScalar {
    fn hash_scalar(&self) -> Scalar;
}

impl HashesToScalar for Vec<u8> {
    /// Hashes a vector of bytes to a Scalar
    ///
    /// First hashes the byte vector to 64 bytes
    /// Then parses those bytes as a Scalar:
    /// "Converts a 512-bit little endian integer into a Scalar by reducing by the modulus."
    fn hash_scalar(&self) -> Scalar {
        let hash = hash_to_64_bytes(&self);
        Scalar::from_bytes_wide(&hash)
    }
}

impl HashesToScalar for [u8] {
    /// Hashes an array of bytes to a Scalar
    ///
    /// First hashes the byte array to 64 bytes
    /// Then parses those bytes as a Scalar:
    /// "Converts a 512-bit little endian integer into a Scalar by reducing by the modulus."
    fn hash_scalar(&self) -> Scalar {
        let hash = hash_to_64_bytes(&self);
        Scalar::from_bytes_wide(&hash)
    }
}

/// Pseudo random function with output mapped to Scalar
///
/// Uses HMac<Sha512> to derive 64 bytes which get interpreted as a scalar
///
/// Converting to scalar:
/// "Converts a 512-bit little endian integer into a Scalar by reducing by the modulus."
///
/// ## Arguments
/// * `key` - Key bytes
/// * `msg` - Msg bytes
///
/// ## Output
/// * Scalar element
pub fn prf(key: &[u8], msg: &[u8]) -> Scalar {
    let bytes = rand::prf(key, msg);
    Scalar::from_bytes_wide(&bytes)
}

/// Syntacting sugar to pair two references to Projective elements
///
/// Saves you from converting them to Affine elements each time
/// ## Arguments
/// * `g1` - Reference to a G1Projective element
/// * `g2` - Reference to a G2Projective element
///
/// ## Output
/// * Gt element corresponding to the pairing of the input elements
///
pub fn pair_proj(g1: &G1Projective, g2: &G2Projective) -> Gt {
    pairing(&G1Affine::from(g1), &G2Affine::from(g2))
}

impl AddVecs for Vec<Scalar> {
    fn add_vec(&self, rhs: &Self) -> Vec<Scalar> {
        let mut res = self.clone();
        for i in 0..res.len() {
            res[i] += rhs[i];
        }
        res
    }
    fn add_vec_mut(&mut self, rhs: &Self) {
        for i in 0..self.len() {
            self[i] += rhs[i];
        }
    }
}

impl ScaleVec<Scalar> for Vec<Scalar> {
    fn scale(&self, factor: &Scalar) -> Self {
        let mut res = self.clone();
        for i in 0..self.len() {
            res[i] *= factor;
        }
        res
    }
    fn scale_mut(&mut self, factor: &Scalar) {
        for i in 0..self.len() {
            self[i] *= factor;
        }
    }
}

impl NumsToVector<Vec<Scalar>> for Vec<u64> {
    fn nums_to_vector(&self) -> Vec<Scalar> {
        let mut res = Vec::new();
        for item in self {
            res.push(Scalar::from(*item));
        }
        res
    }
}

impl AugmentFile for Vec<Vec<Scalar>> {
    fn augment(&self, in_front: bool) -> Self {
        let file_size = self.len();
        let unit = vec![Scalar::zero(); file_size];
        let mut res = Vec::new();
        for i in 0..file_size {
            let mut curunit = unit.clone();
            curunit[i] = Scalar::one();
            let mut msg = self[i].clone();
            if in_front {
                curunit.append(&mut msg);
                res.push(curunit);
            } else {
                msg.append(&mut curunit);
                res.push(msg);
            }
        }
        res
    }
}

pub fn msgu64_to_msg_scalar(msg: &Vec<u64>) -> Vec<Scalar> {
    msg.iter().map(|m| Scalar::from(*m)).collect()
}
pub fn fileu64_to_file_scalar(file: &Vec<Vec<u64>>) -> Vec<Vec<Scalar>> {
    file.iter()
        .map(|packet| msgu64_to_msg_scalar(packet))
        .collect()
}

impl<R: RngCore, G: Group> GenRandomVec<G> for R{
    fn gen_rand_vec(&mut self, n: usize) -> Vec< G> {
        (0..n).map(|_| G::random(&mut *self)).collect()
    }
}
