use sha2::{Digest, Sha512};

/// Hashses a byte slice to 64 bytes
///
/// Uses Sha512 as a hash function
///
/// ## Arguments
/// * `input_bytes` - The byte slice to be hashed
///
/// ## Output
/// * An array of 64 bytes
pub fn hash_to_64_bytes(input_bytes: &[u8]) -> [u8; 64] {
	let mut hasher = Sha512::new();
	hasher.update(input_bytes);
	let mut output = [0u8; 64];
	output.copy_from_slice(&hasher.finalize());
	output
}
