/**
 * We define a packet as a list of something.
 * In practice, this is either a u64 or a scalar.
 * The name packet is taken from the network coding settings,
 * in which packets (parts of a file) are signed and combined.
 */
pub type Packet<T> = Vec<T>;
/**
 * We define a file as a list of packets of a certain type
 * Inspired by network coding, we use the name file.
 * The file is the thing that needs to be send across a network.
 * It is split into multiple packets, which are each signed.
 * When combined, the packets turn back into the original file.
 */
pub type File<T> = Vec<Packet<T>>;
