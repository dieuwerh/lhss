use crate::ff::Field;
use crate::group::Group;
use crate::misc::bls_util::pair_proj;
use crate::misc::bls_util::{HashesToG1, HashesToScalar};
use crate::misc::hashing::hash_to_64_bytes;
use crate::misc::types::File;
use bls12_381::G1Projective;
use bls12_381::G2Projective;
use bls12_381::Gt;
use bls12_381::Scalar;
use rand::{thread_rng, RngCore};

#[derive(Clone)]
pub struct Lin2021 {
    gs: Vec<G1Projective>,
    h: G2Projective,
}

impl Lin2021 {
    fn h1(input_bytes: &[u8]) -> G1Projective {
        input_bytes.hash_g1()
    }
    fn h2(input_vec_msg: &Vec<Scalar>) -> G1Projective {
        let input_bytes: Vec<Vec<u8>> = input_vec_msg
            .iter()
            .map(|msg_part| msg_part.to_bytes().to_vec())
            .collect();
        let input_bytes = input_bytes.concat();
        Self::h1(&input_bytes)
    }
    fn h3(input_gt: &Gt) -> G1Projective {
        input_gt.hash_g1()
    }
    fn h(input_vec_msg: &Vec<Scalar>, input_gt: &Gt) -> Scalar {
        let input_bytes: Vec<[u8; 32]> = input_vec_msg.iter().map(|msg| msg.to_bytes()).collect();
        let input_bytes = input_bytes.concat();
        let gt_string = input_gt.to_string();
        let gt_bytes = gt_string.as_bytes();
        let input_bytes = [&input_bytes, gt_bytes].concat();
        let scalar_hash_input = hash_to_64_bytes(&input_bytes[..]);
        (&scalar_hash_input).hash_scalar()
    }
}

/// Signature Scheme by Lin et al. (2021)
impl Lin2021 {
    /// Setup function to create "common parameters"
    /// Chooses a random generators in G1 'packet length' times
    /// and 1 generator in G2
    /// The 4 hash functions are already implemented on this struct
    pub fn setup(packet_length: usize, rng: &mut impl RngCore) -> Self {
        let gs = vec![G1Projective::random(&mut *rng); packet_length];
        let h = G2Projective::random(&mut *rng);
        Self { gs, h }
    }
    /// Generates a key pair
    /// Selects a random Scalar element as the secret key
    /// Sets the public key to h * sk
    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let sk = Scalar::random(rng);
        let pk = self.h * sk;
        Key { sk, pk }
    }
    /// Signs a message
    /// @sk_a: Secret key of signer A
    /// pk_b: Public Key of combiner B
    /// id: file identifier of things this signature can be combined with
    /// m: number of parts this signature will be combined with in total
    /// msg: The message to be signed, which is a list of scalars
    pub fn sign(
        &self,
        sk_a: &Scalar,
        pk_b: &G2Projective,
        id: &[u8],
        m: usize,
        msg: &Vec<Scalar>,
    ) -> G1Projective {
        let mut signer_part = G1Projective::identity();
        let n = msg.len() - m;
        for i in 0..m {
            if msg[i + n] == Scalar::zero() {
                continue;
            }
            let i_bytes = i.to_be_bytes();
            let input_bytes = [id, &i_bytes].concat();
            signer_part += Lin2021::h1(&input_bytes) * msg[i + n];
        }
        for j in 0..n {
            signer_part += self.gs[j] * msg[j];
        }
        signer_part *= sk_a;
        let combiner_part = Lin2021::h2(msg);
        let combiner_part = pair_proj(&combiner_part, pk_b);
        let combiner_part = combiner_part * sk_a;
        let combiner_part = Lin2021::h3(&combiner_part);
        signer_part + combiner_part
    }
    /// Designated verify of a message
    /// pk_a: Public key of the signer A
    /// sk_b: Private key of the combiner
    /// id: file identifier of things this signature can be combined with
    /// m: number of parts this siganture will be combined with in total
    /// sigd: a designated signature
    /// msg: the message that was signed
    pub fn dverify(
        &self,
        pk_a: &G2Projective,
        sk_b: &Scalar,
        id: &[u8],
        m: usize,
        sigd: &G1Projective,
        msg: &Vec<Scalar>,
    ) -> bool {
        let p1 = pair_proj(sigd, &self.h);

        let n = msg.len() - m;
        let mut signer_part = G1Projective::identity();
        for i in 0..m {
            let augment_part = msg[i + n];
            if augment_part != Scalar::zero() {
                let input_bytes = [id, &i.to_be_bytes()].concat();
                signer_part += Lin2021::h1(&input_bytes) * augment_part;
            }
        }
        for j in 0..n {
            signer_part += self.gs[j] * msg[j];
        }
        let signer_part = pair_proj(&signer_part, pk_a);
        let combiner_part = Lin2021::h2(msg);
        let combiner_part = pair_proj(&combiner_part, pk_a);
        let combiner_part = combiner_part * sk_b;
        let combiner_part = Lin2021::h3(&combiner_part);
        let combiner_part = pair_proj(&combiner_part, &self.h);
        let p2 = signer_part + combiner_part;

        p1 == p2
    }
    /// Combines multiple messages
    /// Signed by A who designated B to combine the messages
    /// pk_a: Public Key of signer A
    /// sk_b: Secret key of the combiner B
    /// id: file identifier
    /// msgs: l messages
    /// sigs: l designated signatures
    pub fn combine(
        &self,
        pk_a: &G2Projective,
        sk_b: &Scalar,
        // id: &[u8],
        msgs: &Vec<Vec<Scalar>>,
        sigs: &Vec<G1Projective>,
        f: &Vec<Scalar>,
    ) -> (Vec<Scalar>, Scalar, G1Projective) {
        let mut msg_res = vec![Scalar::zero(); msgs[0].len()];
        let mut sig_res = G1Projective::identity();
        let l = msgs.len();
        for k in 0..l {
            let coe = f[k];
            let msg = &msgs[k];
            let sig = sigs[k];
            let scaled_msg = msg.scale(&coe);
            msg_res.add_vec(&scaled_msg);

            let combiner_part = Lin2021::h2(msg);
            let combiner_part = pair_proj(&combiner_part, pk_a) * sk_b;
            let combiner_part = Lin2021::h3(&combiner_part);
            let combiner_part_inverse = -combiner_part;
            sig_res += (sig + combiner_part_inverse) * coe;
        }

        let mut rng = thread_rng();
        let c = Scalar::random(&mut rng);
        let big_r = pair_proj(&self.gs[0], &self.h) * c;
        let big_s = Lin2021::h(&msg_res, &big_r);
        let big_t = (sig_res * big_s) + (self.gs[0] * c);
        (msg_res, big_s, big_t)
    }
    /// Verify: verify the combined signature
    ///
    pub fn verify(
        &self,
        pk_a: &G2Projective,
        id: &[u8],
        m: usize,
        msg: &Vec<Scalar>,
        big_s: &Scalar,
        big_t: &G1Projective,
    ) -> bool {
        let n = msg.len() - m;
        let mut sign_part = G1Projective::identity();
        for i in n..m {
            let input_bytes = [id, &i.to_be_bytes()].concat();
            let hash = Lin2021::h1(&input_bytes);
            sign_part += hash * msg[i];
        }
        for j in 0..n {
            sign_part += self.gs[j] * msg[j];
        }
        let mut big_r = pair_proj(big_t, &self.h);
        big_r += pair_proj(&sign_part, &-(pk_a)) * big_s;
        big_s == &Lin2021::h(&msg, &big_r)
    }
}

impl Lin2021 {
    pub fn sign_file_single_key(
        &self,
        sk_a: &Scalar,
        pk_b: &G2Projective,
        file_id: &[u8],
        file: &File<Scalar>,
    ) -> Vec<G1Projective> {
        let num_packets_in_file: usize = file.len();
        file.iter()
            .map(|packet| self.sign(sk_a, pk_b, file_id, num_packets_in_file, packet))
            .collect()
    }
    pub fn sign_file_multiple_keys(
        &self,
        sk_as: &Vec<Scalar>,
        pk_bs: &Vec<G2Projective>,
        file_id: &[u8],
        file: File<Scalar>,
    ) -> Vec<G1Projective> {
        let num_packets_in_file: usize = file.len();
        sk_as
            .iter()
            .zip(pk_bs)
            .zip(file)
            .map(|((sk_a, pk_b), msg)| self.sign(sk_a, pk_b, file_id, num_packets_in_file, &msg))
            .collect()
    }

    pub fn verify_file_single_vk(
        &self,
        pk_a: &Pk,
        sk_b: &Sk,
        file_id: &[u8],
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .map(|(packet, sig)| self.dverify(pk_a, sk_b, file_id, file.len(), sig, packet))
            .collect()
    }

    pub fn verify_file(
        &self,
        pk_as: &Vec<Pk>,
        sk_bs: &Vec<Sk>,
        file_id: &[u8],
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        pk_as
            .iter()
            .zip(sk_bs)
            .zip(file)
            .zip(sigs)
            .map(|(((pk_a, sk_b), packet), sig)| {
                self.dverify(pk_a, sk_b, file_id, file.len(), sig, packet)
            })
            .collect()
    }
}

pub type Sk = Scalar;
pub type Pk = G2Projective;

#[derive(Clone, Copy)]
pub struct Key {
    pub sk: Scalar,
    pub pk: G2Projective,
}
pub type Sig = G1Projective;
pub trait VecAdd {
    fn add_vec(&mut self, rhs: &Self);
}
impl VecAdd for Vec<Scalar> {
    fn add_vec(&mut self, rhs: &Self) {
        for i in 0..rhs.len() {
            self[i] += rhs[i];
        }
    }
}
pub trait VecScale {
    fn scale(&self, scale: &Scalar) -> Self;
}
impl VecScale for Vec<Scalar> {
    fn scale(&self, scale: &Scalar) -> Vec<Scalar> {
        let mut res = self.clone();
        for i in 0..res.len() {
            res[i] *= scale;
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use rand_chacha::ChaChaRng;

    use super::*;

    #[test]
    fn sign_dverify() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = Lin2021::setup(1, &mut rng);
        let key_a = sch.keygen(&mut rng);
        let key_b = sch.keygen(&mut rng);
        let id = b"signature scheme test";
        let msg = vec![Scalar::from(123)];
        let sigd = sch.sign(&key_a.sk, &key_b.pk, &id[..], 1, &msg);
        let ver = sch.dverify(&key_a.pk, &key_b.sk, &id[..], 1, &sigd, &msg);
        assert!(ver);
    }
    #[test]
    fn lin21_sign_double_verify() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = Lin2021::setup(1, &mut rng);
        let key_a = sch.keygen(&mut rng);
        let key_b = sch.keygen(&mut rng);
        let id = b"signature scheme test";
        let msg = vec![Scalar::from(123)];
        let sigd = sch.sign(&key_a.sk, &key_b.pk, &id[..], 1, &msg);
        let (double_msg, s, t) = sch.combine(
            &key_a.pk,
            &key_b.sk,
            // &id[..],
            &vec![msg],
            &vec![sigd],
            &vec![Scalar::from(2)],
        );

        let ver = sch.verify(&key_a.pk, &id[..], 1, &double_msg, &s, &t);
        assert!(ver);
    }
}
