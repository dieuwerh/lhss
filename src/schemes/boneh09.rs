use crate::ff::Field;
use crate::group::Group;
use crate::misc::bls_util::pair_proj;
use crate::misc::bls_util::HashesToG1;
use crate::misc::types::File;
use bls::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Scalar};
use rand::RngCore;

/***
 * Signature scheme by
 * @authors: Boneh, Freeman, Katz, Waters
 * @title: 'Signing a Linear Subspace: Signature Schemes for Network Coding'
 * @year: 2009
 * @model: Random Oracle Model
 * @assumptions: computational Diffie-Hellman in bilinear groups
 * @message: vector
 * @claims:
 * @remarks: pub key and per vector signatures are of constant size
 * can be used for streamed data
 * @doi https://doi.org/10.1007/978-3-642-00468-1_5
 */

// In the paper this scheme is named NCS1
pub struct Bon09 {
    h: G2Affine,
    gs: Vec<G1Projective>,
}
impl Bon09 {
    /// Create istance of scheme
    ///
    /// ## Arguments
    /// * `packet_length`: usize indicating number of items in a packet
    /// * `rng` - mutable instance of RngCore, used to select random generators of g1 and g2
    ///
    /// ## Operations
    /// * Choose |packet| generators g_i <- G1
    /// * Choose a generator h <- G2
    pub fn setup(packet_length: usize, rng: &mut impl RngCore) -> Self {
        let gs = (0..packet_length)
            .map(|_| G1Projective::random(&mut *rng))
            .collect();
        Self {
            h: G2Affine::generator(),
            gs,
        }
    }
    /// Generate a key pair
    ///
    /// ## Arguments
    /// * `rng` - mutable instance of RngCore, used to select a random scalar
    ///
    /// ## Operations
    /// * Select random signing key a <- Z_q*
    /// * Compute verification key u = h^a in G2
    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let a = Scalar::random(rng);
        let u = self.h * a;
        Key { sk: a, pk: u }
    }
    /// Signs a packet under a certain file id using the provided secret key
    ///
    /// ## Arguments
    /// * `sk` - Scalar, Secret key used for signing
    /// * `id_bytes` - bytes that identify this file
    /// * `packet` - list of scalars, data to be signed
    ///
    /// ## Operations
    /// * sig <- (
    ///   ∏ i=1..m (
    ///     H(id, i)^(packet\[n+1])
    /// ) * ∏ j=1..n (
    ///     g_j ^(packet\[j]
    ///  )
    /// ) ^ sk
    ///
    /// ## Output
    /// * A signature on the provided packet and id bytes: an element of G1
    pub fn sign(&self, sk: &Sk, id_bytes: &[u8], packet: &Vec<Scalar>) -> Sig {
        let n = self.gs.len();
        let m = packet.len() - n;
        let mut augment_part = G1Projective::identity();
        let sc_zero = Scalar::zero();
        // Loop over the unit vector, perform hash operation when current bit is one.
        for i in 0..m {
            let augment_bit = packet[n + i];
            // Only one bit of the augment bart should be one, the rest should be zero, since it is a unit vector that is appended
            if augment_bit != sc_zero {
                let hash = Bon09::h(id_bytes, &i.to_be_bytes());
                augment_part += hash * augment_bit;
            }
        }
        // Loop over the bytes in the packet. Exponentiate the j'th g by the j'th part of the packet
        // Add the result to the running total
        let mut msg_part = G1Projective::identity();
        for j in 0..n {
            let cur_msg_part = self.gs[j] * packet[j];
            msg_part += cur_msg_part;
        }
        // Use the secret key to bind the augment part and the message part to the signer
        let sig = (augment_part + msg_part) * sk;
        sig
    }
    /// Combines signatures by raising them to provided coefficients and summing them together
    ///
    /// ## Arguments
    /// * `f` - list of coefficients: Vec<Scalar>
    /// * `sigs` - list of signatures: Vec<G1>
    ///
    /// ## Operations
    /// sig <- ∏ i=1..l sig_i^(f_i)
    ///
    /// ## Output
    /// * A signature: G1
    pub fn combine(&self, f: &Vec<Scalar>, sigs: &Vec<Sig>) -> Sig {
        let mut sig = G1Projective::identity();
        for i in 0..sigs.len() {
            sig += sigs[i] * f[i];
        }
        sig
    }
    /// Verifies a message and signature with a public key
    ///
    /// ## Arguments
    /// * `pk` - the public key to verify the signature with: G2
    /// * `id_bytes` - The id of the file that was signed
    /// * `packet` - The packet that was signed: Vec<Scalar>
    /// * `sig` - The signature to verify: G1
    ///
    /// ## Operations
    ///  e(sig, h) ==
    /// e (
    ///     ∏ i=1..m ( H(id, i)^(packet\[n+1]))
    ///     * ∏ j=1..n ( g_j ^(packet\[j])
    /// ), vk)
    ///
    /// ## Output
    /// * bool indicating if verification succeeded or not
    pub fn verify(&self, pk: &Pk, id_bytes: &[u8], packet: &Vec<Scalar>, sig: &Sig) -> bool {
        let n = self.gs.len();
        let m = packet.len() - n;
        let mut check_msg = G1Projective::identity();
        let sc_zero = Scalar::zero();
        // Reconstruct the signature up until the binding with the secret key part
        for i in 0..m {
            let augment_byte = packet[n + i];
            if augment_byte != sc_zero {
                let hash = Bon09::h(id_bytes, &i.to_be_bytes());
                check_msg += hash * augment_byte;
            }
        }
        for j in 0..n {
            let cur_msg_part = self.gs[j] * packet[j];
            check_msg += cur_msg_part;
        }
        // Pair the signature with the g2 generator
        let signature_pair = pairing(&G1Affine::from(sig), &self.h);
        // Pair the reconstructed signature with the public key
        let reconstructed_pair = pair_proj(&check_msg, &pk);
        // Check equality
        signature_pair == reconstructed_pair
    }
}

impl Bon09 {
    pub fn sign_file_single_key(&self, sk: &Sk, id: &[u8], file: &File<Scalar>) -> Vec<Sig> {
        file.iter()
            .map(|packet| self.sign(sk, id, packet))
            .collect()
    }

    pub fn sign_file_multiple_keys(
        &self,
        sks: &Vec<Sk>,
        id: &[u8],
        file: &File<Scalar>,
    ) -> Vec<Sig> {
        sks.iter()
            .zip(file)
            .map(|(sk, packet)| self.sign(sk, id, packet))
            .collect()
    }

    pub fn verify_file_single_vk(
        &self,
        pk: &Pk,
        file_id: &[u8],
        file: &File<Scalar>,
        signatures: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(signatures)
            .map(|(packet, sig)| self.verify(pk, &file_id, packet, sig))
            .collect()
    }

    pub fn verify_file(
        &self,
        pks: &Vec<Pk>,
        file_id: &[u8],
        file: &File<Scalar>,
        signatures: &Vec<Sig>,
    ) -> Vec<bool> {
        pks.iter()
            .zip(file)
            .zip(signatures)
            .map(|((vk, packet), sig)| self.verify(vk, &file_id, packet, sig))
            .collect()
    }
}

impl Bon09 {
    /// Hash function to hash a pair of arbitrary length byte arrays to a G1 element
    ///
    /// ## Arguments
    /// * `input_bytes_0` - first input bytes
    /// * `input_bytes_1` - second input bytes
    ///
    /// ## Output
    /// * G1
    fn h(input_bytes_0: &[u8], input_bytes_1: &[u8]) -> G1Projective {
        let input_bytes = [input_bytes_0, input_bytes_1].concat();
        input_bytes.hash_g1()
    }
}

pub type Sk = Scalar;
pub type Pk = G2Projective;
pub type Id = u8;
pub type MSG = Scalar;
pub type Vector = Vec<MSG>;
pub type Sig = G1Projective;
pub type Coeff = Scalar;
#[derive(Clone, Copy)]
pub struct Key {
    pub sk: Sk,
    pub pk: Pk,
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;

    use super::*;
    use crate::misc::num_traits::*;

    #[test]
    fn bon_sign_verify_basic() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let msg_length = 1;
        let sch = Bon09::setup(msg_length, &mut rng);
        let key = sch.keygen(&mut rng);
        let id = b"bon sign verify id";
        let msg = vec![Scalar::random(&mut rng); msg_length];
        let msg = [msg, vec![Scalar::from(1)]].concat();
        let sig = sch.sign(&key.sk, &id[..], &msg);
        let ver = sch.verify(&key.pk, &id[..], &msg, &sig);
        assert!(ver);
    }
    #[test]
    fn bon_sign_verify_msglegnth_5() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let msg_length = 5;
        let sch = Bon09::setup(msg_length, &mut rng);
        let key = sch.keygen(&mut rng);
        let id = b"bon sign verify id";
        let msg = vec![Scalar::random(&mut rng); msg_length];
        let msg = [msg, vec![Scalar::from(1)]].concat();
        let sig = sch.sign(&key.sk, &id[..], &msg);
        let ver = sch.verify(&key.pk, &id[..], &msg, &sig);
        assert!(ver);
    }
    #[test]
    fn bon_sign_double_verify() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let msg_length = 1;
        let sch = Bon09::setup(msg_length, &mut rng);
        let key = sch.keygen(&mut rng);
        let id = b"bon sign verify id";
        let msg = vec![Scalar::random(&mut rng); msg_length];
        let msg = [msg, vec![Scalar::from(1)]].concat();
        let sig = sch.sign(&key.sk, &id[..], &msg);
        let f = vec![Scalar::from(2)];
        let doubled = msg.scale(&f[0]);
        let doubled_sig = sch.combine(&f, &vec![sig]);
        let ver = sch.verify(&key.pk, &id[..], &doubled, &doubled_sig);
        assert!(ver);
    }
    #[test]
    fn bon_sign_combine_verify() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let msg_length = 5;
        let sch = Bon09::setup(msg_length, &mut rng);
        let key = sch.keygen(&mut rng);
        let id = b"bon sign verify id";
        let msg = vec![Scalar::random(&mut rng); msg_length];
        let msg = [msg, vec![Scalar::from(1), Scalar::zero()]].concat();
        let sig = sch.sign(&key.sk, &id[..], &msg);
        let msg2 = vec![Scalar::random(&mut rng); msg_length];
        let msg2 = [msg2, vec![Scalar::zero(), Scalar::from(1)]].concat();
        let sig2 = sch.sign(&key.sk, &id[..], &msg2);
        let f = vec![Scalar::from(1), Scalar::from(1)];
        let added = msg.add_vec(&msg2);
        let added_sig = sch.combine(&f, &vec![sig, sig2]);
        let ver = sch.verify(&key.pk, &id[..], &added, &added_sig);
        assert!(ver);
    }

    #[test]
    fn file_sign_ver() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let packet_length = 2;
        let file_length = 2;
        let sch = Bon09::setup(packet_length, &mut rng);
        let key = sch.keygen(&mut rng);
        let id = b"filesign";
        let file: File<Scalar> = (0..file_length)
            .map(|_| {
                (0..packet_length)
                    .map(|_| Scalar::random(&mut rng))
                    .collect()
            })
            .collect();
        let sigs = sch.sign_file_single_key(&key.sk, &id[..], &file);
        let checks = sch.verify_file_single_vk(&key.pk, &id[..], &file, &sigs);
        assert_eq!(checks, vec![true; 2]);
    }
}
