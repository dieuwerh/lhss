use bls::hash_to_curve::HashToCurve;
use bls::{G1Projective, G2Projective, Scalar};
use bls12_381::hash_to_curve::ExpandMsgXmd;
use bls12_381::Gt;
use ff::Field;
use group::Group;
use rand::RngCore;

use crate::misc::bls_util::pair_proj;
use crate::misc::num_traits::{AddVecs, ScaleVec};
use crate::misc::types::File;

/***
 * Signature scheme by
 * @authors: Lin, Huang, Li, Wu, Yang
 * @title: 'Linearly Homomorphic Signatures with Designated Entities'
 * @year: 2017
 * @model: ROM
 * @assumptions: Gap Bilinear Diffie-Hellman
 * @message: Vectors
 */

/// Instance of Lin 2017 Scheme
///
/// * `n` - Usize indicating the number of items in a packet
/// * `m` - Usize indicating the number of packets in a file
/// * `h` - Generator of G2 used for key generation and pairing
/// * `gs` - n generators of G1, used for signing
pub struct Lin17 {
    n: usize,
    m: usize,
    h: G2Projective,
    gs: Vec<G1Projective>,
}
impl Lin17 {
    /// Create instance of scheme
    ///
    /// ## Arguments
    /// * `packet_length`: usize indicating number of items in a packet
    /// * `file_length`: usize indicating how many packets are in a file
    /// * `rgn`: mutable instance of Rngcore, used to select random generators
    pub fn setup(packet_length: usize, file_length: usize, rng: &mut impl RngCore) -> Self {
        let h = G2Projective::random(&mut *rng);
        let gs = vec![G1Projective::random(&mut *rng); packet_length];
        Self {
            n: packet_length,
            m: file_length,
            h,
            gs,
        }
    }

    /// Generate a key pair
    ///
    /// ## Arguments
    /// * `rng` - mutable instance of RngCore, used to select a random scalar
    ///
    /// ## Output
    /// * A key pair consisting of a public key: G2, and a secret key: Scalar
    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let a = Scalar::random(rng);
        let u = self.h * a;
        Key { sk: a, pk: u }
    }

    /// Signs a packet under a certain file id using the provided secret key and the public key of the designated combiner
    ///
    /// ## Arguments
    /// * `sk_a` - The secret key of the signer
    /// * `pk_b` - The public key of the designated combiner
    /// * `id` - Id of the file to be signed
    /// * `packet` - list of scalars, data to be signed
    ///
    /// ## Output
    /// A signature on the provided packet
    pub fn sign(&self, sk_a: &Sk, pk_b: &Pk, id: &[u8], packet: &Vec<Scalar>) -> Sig {
        let mut res = self.prepare(id, packet);
        res = res * sk_a;
        let msg_hash = Self::h2(packet);
        let pair = pair_proj(&msg_hash, pk_b) * sk_a;
        let msg_designator_hash = Self::h3(&pair);
        res += msg_designator_hash;
        res
    }

    /// Verifies a packet and signature for a designated combiner
    ///
    /// ## Arguments
    /// * `pk_a` - the public key of the signer
    /// * `sk_b` - the secret key of the designated combiner
    /// * `id` - the id of the file this packet belongs to
    /// * `sig` - The signature to verify: G1
    /// * `packet` - The packet that was signed: Vec<Scalar>
    ///
    /// ## Output
    /// A boolean indicating if verification succeeded or not   
    pub fn verify(&self, pk_a: &Pk, sk_b: &Sk, id: &[u8], sig: &Sig, msg: &Vec<Scalar>) -> bool {
        let gam1 = pair_proj(sig, &self.h);

        let gam2in = self.prepare(id, msg);
        let p1 = pair_proj(&gam2in, pk_a);

        let msg_hash = Self::h2(msg);
        let p2 = pair_proj(&msg_hash, pk_a) * sk_b;
        let msg_designator_hash = Self::h3(&p2);
        let p3 = pair_proj(&msg_designator_hash, &self.h);

        let gam2 = p1 + p3;
        gam1 == gam2
    }

    /// Combines signatures signed by entity a, designated to combine by entity b
    /// Signatures will be combined as to only be verified by entity c, the designated verifier
    ///
    /// ## Arguments
    /// * `pk_a` - the public key of the signer
    /// * `pk_c` - the public key of the designated verifier
    /// * `sk_b` - the secret key of the designated combiner
    /// * `f` - list of coefficients: Vec<Scalar>
    /// * `packets` - the packets to be combined
    /// * `sigs` - list of signatures to be combined: Vec<G1>
    ///
    /// ## Output
    /// * The outcome of the function f applied to the packets, and the corresponding designated signature
    pub fn combine(
        &self,
        pk_a: &Pk,
        pk_c: &Pk,
        sk_b: &Sk,
        f: &Vec<Scalar>,
        msgs: &Vec<Vec<Scalar>>,
        sigs: &Vec<Sig>,
    ) -> (Vec<Scalar>, DSig) {
        let scaled_msgs: Vec<Vec<Scalar>> =
            msgs.iter().zip(f).map(|(m, fi)| m.scale(&fi)).collect();
        let summed = scaled_msgs
            .into_iter()
            .reduce(|accum, item| accum.add_vec(&item))
            .unwrap();

        let mut combres = G1Projective::identity();
        for i in 0..f.len() {
            let m = &msgs[i];
            let fi = f[i];
            let sig = sigs[i];
            let msg_hash = Self::h2(m);
            let pair = pair_proj(&msg_hash, pk_a) * sk_b;
            let hash = Self::h3(&pair);
            // let hashneg = -hash;
            let comb = (sig - hash) * fi;
            combres += comb;
        }
        let designated_combined_signature = pair_proj(&(combres), pk_c);
        (summed, designated_combined_signature)
    }

    /// Verifies a packet and a designated signature on it
    ///
    /// ## Arguments
    /// * `pk_a` - the public key of the signer
    /// * `sk_c` - the secret key of the designated verifier
    /// * `id` - the id of the file this packet belongs to
    /// * `sig` - The signature to verify: G1
    /// * `packet` - The packet that was signed: Vec<Scalar>
    ///
    /// ## Output
    /// A boolean indicating if verification succeeded or not   
    pub fn dverify(&self, pk_a: &Pk, sk_c: &Sk, id: &[u8], sig: &DSig, msg: &Vec<Scalar>) -> bool {
        let gam = self.prepare(id, msg);
        let pair = pair_proj(&(gam), pk_a) * sk_c;
        &pair == sig
    }
    pub fn simulation(&self, pk_a: &Pk, sk_c: &Sk, id: &[u8], msg: &Vec<Scalar>) -> DSig {
        let hashinput = self.prepare(id, msg);
        pair_proj(&(hashinput), pk_a) * sk_c
    }

    /// Prepares a packet for signing / verifying
    ///
    /// Commonly shared code of both the sign and verify operation
    /// Hashes id and index of unit factor that is 1
    /// and exponentiates gs by parts of the packet
    fn prepare(&self, id: &[u8], packet: &Vec<Scalar>) -> G1Projective {
        let mut res = G1Projective::identity();
        for i in 0..self.m {
            let factor = packet[i + self.n];
            if factor != Scalar::zero() {
                let input_bytes = [id, &i.to_be_bytes()].concat();
                res += Self::h1(&input_bytes) * factor;
            }
        }
        for j in 0..self.n {
            res += self.gs[j] * packet[j];
        }
        res
    }
}

impl Lin17 {
    pub fn sign_file_single_key(
        &self,
        sk_a: &Sk,
        pk_b: &Pk,
        file_id: &[u8],
        file: &File<Scalar>,
    ) -> Vec<Sig> {
        file.iter()
            .map(|packet| self.sign(sk_a, pk_b, file_id, packet))
            .collect()
    }
    pub fn sign_file_multiple_keys(
        &self,
        sk_as: &Vec<Sk>,
        pk_bs: &Vec<Pk>,
        file_id: &[u8],
        file: &File<Scalar>,
    ) -> Vec<Sig> {
        sk_as
            .iter()
            .zip(pk_bs)
            .zip(file)
            .map(|((sk_a, pk_b), packet)| self.sign(sk_a, pk_b, file_id, packet))
            .collect()
    }

    pub fn verify_file_single_vk(
        &self,
        vk_a: &Pk,
        sk_b: &Sk,
        file_id: &[u8],
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .map(|(packet, sig)| self.verify(vk_a, sk_b, file_id, sig, packet))
            .collect()
    }

    pub fn verify_file(
        &self,
        vks: &Vec<Pk>,
        sks: &Vec<Sk>,
        id: &[u8],
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        vks.iter()
            .zip(sks)
            .zip(file)
            .zip(sigs)
            .map(|(((pk_a, sk_b), packet), sig)| self.verify(pk_a, sk_b, id, sig, packet))
            .collect()
    }
}

impl Lin17 {
    /// Hashes bytes to point in G1
    fn h1(input: &[u8]) -> G1Projective {
        let dst: &[u8] = b"tempDST";
        let hashed =
            <G1Projective as HashToCurve<ExpandMsgXmd<sha2::Sha256>>>::encode_to_curve(input, dst);
        hashed
    }
    /// Hashes a vec of scalars to a point in G1
    fn h2(input: &Vec<Scalar>) -> G1Projective {
        // let mut hasher = Sha256::new();
        let bytes = packet_to_bytes(&input);
        // hasher.update(bytes);
        Self::h1(&bytes)
    }
    /// Hashes a point in Gt to a point in G1
    fn h3(input: &Gt) -> G1Projective {
        let string = input.to_string();
        let bytes = string.as_bytes();
        Self::h1(&bytes)
    }
}

/// Helper function to convert a packet (list of scalars) to bytes
fn packet_to_bytes(input: &Vec<Scalar>) -> Vec<u8> {
    let mut res: Vec<u8> = Vec::new();
    for inp in input {
        let bytes = inp.to_bytes();
        res = [res, bytes.to_vec()].concat();
    }
    res
}

/// Secret key: Scalar
pub type Sk = Scalar;
/// Public key: G2 element
pub type Pk = G2Projective;
/// Key pair container
///
/// * `sk` - Secret key: Scalar
/// * `pk` - Public key: G2
#[derive(Debug, Clone)]
pub struct Key {
    pub sk: Sk,
    pub pk: Pk,
}
/// Signature to be combined: G1 element
pub type Sig = G1Projective;
/// Designated signature: Gt element
pub type DSig = Gt;

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use rand_chacha::ChaChaRng;

    use crate::misc::num_traits::{AddVecs, AugmentFile, NumsToVector};

    use super::*;
    #[test]
    fn vec_add_to_self() {
        let x: Vec<Scalar> = vec![Scalar::from(1)];
        let y = x.add_vec(&x);
        assert_eq!(y, vec![Scalar::from(2)])
    }

    #[test]
    fn vec_add_to_other() {
        let x: Vec<Scalar> = vec![Scalar::from(5)];
        let y: Vec<Scalar> = vec![Scalar::from(8)];
        assert_eq!(x.add_vec(&y), vec![Scalar::from(5 + 8)]);
    }

    #[test]
    fn nd_vec_add() {
        let x: Vec<Scalar> = vec![Scalar::from(5), Scalar::from(8)];
        let y: Vec<Scalar> = vec![Scalar::from(8), Scalar::from(5)];
        dbg!(&x);
        dbg!(&y);
        assert_eq!(
            x.add_vec(&y),
            vec![Scalar::from(5 + 8), Scalar::from(5 + 8)]
        );
    }
    #[test]
    fn scale_and_sum() {
        let x: Vec<Scalar> = vec![Scalar::from(5), Scalar::from(8)];
        let y: Vec<Scalar> = vec![Scalar::from(8), Scalar::from(5)];
        let input = vec![x, y];
        // let scaled_msg: Vec<Vector> = input.iter().map(|inp| inp).collect();
        let summed = input
            .into_iter()
            .reduce(|accum, item| accum.add_vec(&item))
            .unwrap();
        println!("summed: {:#?}", summed);
    }

    #[test]
    fn nclin17_single_signature() {
        let mut rng = ChaChaRng::from_entropy();
        let file_size = 1usize;
        let msg_size = 1usize;
        let sch = Lin17::setup(msg_size, file_size, &mut rng);
        let k_a = sch.keygen(&mut rng);
        let k_b = sch.keygen(&mut rng);
        let m1 = vec![1234567u64].nums_to_vector();
        let file = vec![m1];
        let file = file.augment(false);
        let id = b"this is the id for lin2017";
        let id_ref = &id[..];
        let s1 = sch.sign(&k_a.sk, &k_b.pk, id_ref, &file[0]);
        let v1 = sch.verify(&k_a.pk, &k_b.sk, id_ref, &s1, &file[0]);
        assert_eq!(v1, true);
    }

    #[test]
    fn nclin17_combine_two_different() {
        let mut rng = ChaChaRng::from_entropy();
        let file_size = 2usize;
        let msg_size = 1usize;
        let sch = Lin17::setup(msg_size, file_size, &mut rng);
        let k_a = sch.keygen(&mut rng);
        let k_b = sch.keygen(&mut rng);
        let k_c = sch.keygen(&mut rng);
        let m1 = vec![1234567u64].nums_to_vector();
        let m2 = vec![7654321u64].nums_to_vector();
        let file = vec![m1, m2];
        let file = file.augment(false);
        let id = b"this is the id for lin2017";
        let id_ref = &id[..];
        let s1 = sch.sign(&k_a.sk, &k_b.pk, id_ref, &file[0]);
        let s2 = sch.sign(&k_a.sk, &k_b.pk, id_ref, &file[1]);

        let f = vec![Scalar::from(2), Scalar::from(6)];
        let sigs = vec![s1, s2];
        let (combined, dsig) = sch.combine(&k_a.pk, &k_c.pk, &k_b.sk, &f, &file, &sigs);

        let v1 = sch.dverify(&k_a.pk, &k_c.sk, id_ref, &dsig, &combined);
        assert_eq!(v1, true);
    }

    #[test]
    fn nclin17_combine_file() {
        let mut rng = ChaChaRng::from_entropy();
        let file_size = 5usize;
        let msg_size = 1usize;
        let sch = Lin17::setup(msg_size, file_size, &mut rng);
        let k_a = sch.keygen(&mut rng);
        let k_b = sch.keygen(&mut rng);
        let k_c = sch.keygen(&mut rng);
        let id_bytes = b"this is the combined id";
        let id = &id_bytes[..];
        let file = vec![
            vec![123, 456, 789, 101, 112].nums_to_vector(),
            vec![124, 455, 788, 102, 113].nums_to_vector(),
            vec![125, 454, 787, 103, 114].nums_to_vector(),
            vec![126, 453, 785, 104, 115].nums_to_vector(),
            vec![127, 452, 784, 105, 116].nums_to_vector(),
        ];
        let aug_file = file.augment(false);
        let sigs: Vec<Sig> = aug_file
            .iter()
            .map(|msg| sch.sign(&k_a.sk, &k_b.pk, id, msg))
            .collect();
        let f = vec![Scalar::one(); file_size];
        let (summed_msg, summed_dsig) =
            sch.combine(&k_a.pk, &k_c.pk, &k_b.sk, &f, &aug_file, &sigs);
        let ver = sch.dverify(&k_a.pk, &k_c.sk, id, &summed_dsig, &summed_msg);
        assert!(ver);
    }
    #[test]
    fn nclin17_combine_scaled_file() {
        let mut rng = ChaChaRng::from_entropy();
        let file_size = 5usize;
        let msg_size = 1usize;
        let sch = Lin17::setup(msg_size, file_size, &mut rng);
        let k_a = sch.keygen(&mut rng);
        let k_b = sch.keygen(&mut rng);
        let k_c = sch.keygen(&mut rng);
        let id_bytes = b"this is the combined id";
        let id = &id_bytes[..];
        let file = vec![
            vec![123, 456, 789, 101, 112].nums_to_vector(),
            vec![124, 455, 788, 102, 113].nums_to_vector(),
            vec![125, 454, 787, 103, 114].nums_to_vector(),
            vec![126, 453, 785, 104, 115].nums_to_vector(),
            vec![127, 452, 784, 105, 116].nums_to_vector(),
        ];
        let aug_file = file.augment(false);
        let sigs: Vec<Sig> = aug_file
            .iter()
            .map(|msg| sch.sign(&k_a.sk, &k_b.pk, id, msg))
            .collect();
        let f = vec![Scalar::from(123); file_size];
        let (summed_msg, summed_dsig) =
            sch.combine(&k_a.pk, &k_c.pk, &k_b.sk, &f, &aug_file, &sigs);
        let ver = sch.dverify(&k_a.pk, &k_c.sk, id, &summed_dsig, &summed_msg);
        assert!(ver);
    }
}
