use crate::ed::{Signer, Verifier};
use crate::ff::Field;
use crate::group::Group;
use crate::group::GroupEncoding;
use crate::misc::bls_util::{self, pair_proj};
use crate::misc::types::File;
use bls12_381::pairing;
use bls12_381::G1Affine;
use bls12_381::G1Projective;
use bls12_381::G2Affine;
use bls12_381::G2Projective;
use bls12_381::Gt;
use bls12_381::Scalar;
use rand::RngCore;
use std::collections::HashMap;
// use rand::rand_chacha::ChaCha20Rng;

/// Number of bytes for PRF key
/// 16 bytes = 128 bits
/// Corresponds to security of BLS 12 381
const K_BYTE_SIZE: usize = 16;

/// Context Hiding, Multi-Key Linearly Homomorphic Authenticators
/// By Schabhuser, Butin and Buchmann
pub struct Sch2019 {
    pub k: usize, // size of ID space
    n: usize,     // size of the Tag space
    t: usize,     // length of message to be signed
    big_hs: Vec<G1Projective>,
    g1: G1Projective,
    g2: G2Projective,
    gt: Gt, // Generator of gt: pairing of generator g1 and generator g2
}

impl Sch2019 {
    /// Instantiates Schabhuser 2019 MKLH Signature Scheme
    ///
    /// ## Arguments
    /// * `id_space_size` - number indicating how many ids there can be
    /// * `tag_space_size` - nunmber indicating with how many signatures will be combined together
    /// * `msg_length` - number indicating the length of messages vectors to be signed
    pub fn setup(
        id_space_size: usize,
        tag_space_size: usize,
        msg_length: usize,
        rng: &mut impl RngCore,
    ) -> Self {
        let gt = pairing(&G1Affine::generator(), &G2Affine::generator());
        let big_hs = vec![G1Projective::random(&mut *rng); msg_length];
        Sch2019 {
            k: id_space_size,
            n: tag_space_size,
            t: msg_length,
            big_hs,
            g1: G1Projective::generator(),
            g2: G2Projective::generator(),
            gt,
        }
    }
    /// Generates a keypair for the signature scheme
    ///
    /// Uses ChaCha20 as a random number generator
    ///
    /// Uses ed25519 signature scheme as a 'regular' signature scheme
    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let mut k = [0u8; K_BYTE_SIZE];
        rng.fill_bytes(&mut k[..]);
        let k: [u8; K_BYTE_SIZE] = k.try_into().expect("slice with incorrect length");
        let mut ed_rng = rand::rngs::OsRng {};
        let ed_keypair = ed::SigningKey::generate(&mut ed_rng);
        let pk_sig = ed_keypair.verifying_key();
        let xs = vec![Scalar::random(&mut *rng); self.n];
        let y = Scalar::random(&mut *rng);
        let hs: Vec<Gt> = xs.iter().map(|x| self.gt * x).collect();
        let big_y = G2Projective::generator() * y;

        Key {
            sk: Sk {
                k,
                sk_sig: ed_keypair,
                xs,
                y,
            },
            pk: Pk { pk_sig, hs, big_y },
        }
    }
    /// Signs a message
    ///
    /// ## Arguments
    /// * `sk` - Secret key containing a prf key, a ed25519 keypair, and other elements for this signature scheme
    /// * `dataset_id` - The id under which message to be combined are signed. Only messages signed under the same dataset id can be combined together
    /// * `label` - The label containing the singer's id and the tag of this signature
    /// The tag indicates the place in the linear combination program
    /// (e.g. 5a + 7b + 9c has tags 0 for a, 1 for b and 2 for c)
    /// * `msg` - The msg to be signed, which is a vector of the length that was specified during scheme setup
    pub fn sign(
        &self,
        sk: &Sk,
        dataset_id: &[u8],
        label: &Label,
        msg: &Vec<Scalar>,
        rng: &mut impl RngCore,
    ) -> Sig {
        let z = Sch2019::prf(&sk.k, &dataset_id);
        let big_z = self.g2 * z;
        let dataset_msg = [big_z.to_bytes().as_ref(), dataset_id].concat();
        let sigd = sk.sk_sig.try_sign(&dataset_msg).unwrap();
        let r = Scalar::random(&mut *rng);
        let s = Scalar::random(&mut *rng);
        let big_r = self.g1 * (r - s);
        let big_s = self.g2 * (-s);
        let mut big_a = self.g1 * (sk.xs[label.tag] + r);
        let mut big_c = self.g1 * s;
        let mut msg_part = G1Projective::identity();

        for i in 0..self.t {
            msg_part += self.big_hs[i] * msg[i];
        }

        big_a += msg_part;
        big_c += msg_part;
        big_a *= z.invert().unwrap();
        big_c *= sk.y.invert().unwrap();
        Sig {
            lams: vec![Lam {
                id: label.id.clone(),
                sigd,
                big_z,
                big_a,
                big_c,
            }],
            big_r,
            big_s,
        }
    }
    /// Combines signatures together, scaling each one by a certain coefficient specified in f
    ///
    /// ## Arguments
    /// * `f` - A vector containing scalars to multiply signatures with
    /// * `sigs` - A list of signatures to be combined
    ///
    /// ## Examples
    /// * When `f` = [1, ..., 1], the signatures get summed
    /// * When `f` = [a, b, .., z], sigs[0] gets multiplied by a, ... sigs[25] gets multiplied by z
    pub fn combine(&self, f: &Vec<Scalar>, sigs: &Vec<Sig>) -> Sig {
        let mut big_r = G1Projective::identity();
        let mut big_s = G2Projective::identity();
        let mut id_lam_map: HashMap<Vec<u8>, Lam> = HashMap::new();
        for i in 0..f.len() {
            big_r += sigs[i].big_r * f[i];
            big_s += sigs[i].big_s * f[i];

            for lam in &sigs[i].lams {
                let id_entry = id_lam_map
                    .entry(lam.id.clone())
                    .or_insert(lam.clone_empty());
                // (*id_entry).mul_big_a_c(&f[i]);
                (*id_entry).big_a += lam.big_a * f[i];
                (*id_entry).big_c += lam.big_c * f[i];
            }
        }
        let lams: Vec<Lam> = id_lam_map.values().cloned().collect();
        Sig { lams, big_r, big_s }
    }
    /// Verifies a given signature based on the msg and program supplied
    ///
    /// ## Arguments
    /// * `prog` - A multi labeled program containing a dataset id, a list of labels, and a coefficient vector
    /// * `id_pk_map` - A hashmap mapping identities to public keys
    /// * `msg` - The messages of which the signature is being checked
    /// * `sig` - The signature that is being checked
    ///
    /// ## Output
    /// * `bool` result whether the signature checks out or not
    pub fn verify(
        &self,
        prog: &MLProg,
        id_pk_map: &HashMap<Vec<u8>, Pk>,
        msg: &Vec<Scalar>,
        sig: &Sig,
    ) -> bool {
        let mut a_z_pairs = Gt::identity();
        let mut c_y_pairs = Gt::identity();
        for id_index in 0..sig.lams.len() {
            let lam = &sig.lams[id_index];
            let id = &lam.id;
            let pk = id_pk_map.get(id).unwrap();
            let dataset_msg = [lam.big_z.to_bytes().as_ref(), &prog.dataset_id].concat();
            if pk.pk_sig.verify(&dataset_msg, &lam.sigd).is_err() {
                return false;
            }
            a_z_pairs += pair_proj(&lam.big_a, &lam.big_z);
            c_y_pairs += pair_proj(&lam.big_c, &pk.big_y);
        }
        let mut tags_scale_part = Gt::identity();
        let big_r_pair = pair_proj(&sig.big_r, &self.g2);
        let big_s_pair = pair_proj(&self.g1, &sig.big_s);
        for (label, coe) in prog.labels.iter().zip(&prog.f) {
            let pk = id_pk_map.get(&label.id).unwrap();
            tags_scale_part += pk.hs[label.tag] * coe;
        }
        let mut msg_part = G1Projective::identity();
        for i in 0..self.t {
            msg_part += self.big_hs[i] * msg[i];
        }
        let msg_pair = pair_proj(&msg_part, &self.g2);

        let p2 = tags_scale_part + c_y_pairs + big_r_pair;
        let p3 = big_s_pair + c_y_pairs;

        a_z_pairs == p2 && p3 == msg_pair
    }
}

impl Sch2019 {
    pub fn sign_file_single_key(
        &self,
        sk: &Sk,
        dataset_id: &[u8],
        labels: &Vec<Label>,
        file: &File<Scalar>,
        rng: &mut impl RngCore,
    ) -> Vec<Sig> {
        labels
            .iter()
            .zip(file)
            .map(|(label, msg)| self.sign(sk, dataset_id, label, msg, rng))
            .collect()
    }
    pub fn sign_file_multiple_keys(
        &self,
        sks: &Vec<Sk>,
        dataset_id: &[u8],
        labels: &Vec<Label>,
        file: &File<Scalar>,
        rng: &mut impl RngCore,
    ) -> Vec<Sig> {
        sks.iter()
            .zip(labels)
            .zip(file)
            .map(|((sk, label), msg)| self.sign(sk, dataset_id, label, msg, rng))
            .collect()
    }

    pub fn verify_file(
        &self,
        id_vk_map: &HashMap<Vec<u8>, Pk>,
        mlprogs: &Vec<MLProg>,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .zip(mlprogs)
            .map(|((packet, sig), mlprog)| self.verify(mlprog, id_vk_map, packet, sig))
            .collect()
    }
}

impl Sch2019 {
    /// Prf mapping key and msg to a Scalar
    fn prf(key: &[u8], msg: &[u8]) -> Scalar {
        bls_util::prf(key, msg)
    }
}

#[derive(Clone)]
pub struct Sk {
    k: [u8; K_BYTE_SIZE],
    sk_sig: ed::SigningKey,
    xs: Vec<Scalar>,
    y: Scalar,
}
#[derive(Clone)]
pub struct Pk {
    pk_sig: ed::VerifyingKey,
    hs: Vec<Gt>,
    big_y: G2Projective,
}
#[derive(Clone)]
pub struct Key {
    pub sk: Sk,
    pub pk: Pk,
}
#[derive(Clone)]
pub struct Label {
    pub id: Vec<u8>,
    pub tag: usize,
}
impl Label {
    pub fn new(id: Vec<u8>, tag: usize) -> Self {
        Self { id, tag }
    }
}
#[derive(Clone, Debug)]
pub struct Lam {
    id: Vec<u8>,
    sigd: ed::Signature,
    big_z: G2Projective,
    big_a: G1Projective,
    big_c: G1Projective,
}
impl Lam {
    fn clone_empty(&self) -> Lam {
        let mut res = self.clone();
        res.big_a = G1Projective::identity();
        res.big_c = G1Projective::identity();
        res
    }
}
#[derive(Clone)]
pub struct Sig {
    lams: Vec<Lam>,
    big_r: G1Projective,
    big_s: G2Projective,
}
pub struct MLProg {
    pub dataset_id: Vec<u8>,
    pub labels: Vec<Label>,
    pub f: Vec<Scalar>,
}
impl MLProg {
    pub fn new(dataset_id: &Vec<u8>, labels: &Vec<Label>, coefficients: &Vec<Scalar>) -> Self {
        Self {
            dataset_id: dataset_id.to_vec(),
            labels: labels.clone(),
            f: coefficients.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;
    use rand_chacha::ChaChaRng;

    use super::*;

    #[test]
    fn schab19_sign_verify() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = Sch2019::setup(20, 1, 1, &mut rng);
        let key = sch.keygen(&mut rng);
        let msg = vec![Scalar::from(16)];
        let id = b"signedbydieuwerh";
        let did = b"testdatasetid";
        let label = Label {
            tag: 0,
            id: id.to_vec(),
        };
        let prog = MLProg {
            dataset_id: did.to_vec(),
            labels: vec![label],
            f: vec![Scalar::one()],
        };
        let sig = sch.sign(&key.sk, &prog.dataset_id, &prog.labels[0], &msg, &mut rng);
        let mut id_pk_map = HashMap::new();
        id_pk_map.insert(id.to_vec(), key.pk.clone());
        let ver = sch.verify(&prog, &id_pk_map, &msg, &sig);

        assert!(ver)
    }

    #[test]
    fn schab19_sign_double_verify() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = Sch2019::setup(20, 1, 1, &mut rng);
        let key = sch.keygen(&mut rng);
        let msg = vec![Scalar::from(16)];
        let id = b"signedbydieuwerh";
        let did = b"testdatasetid";
        let label = Label {
            tag: 0,
            id: id.to_vec(),
        };
        let prog = MLProg {
            dataset_id: did.to_vec(),
            labels: vec![label],
            f: vec![Scalar::from(2)],
        };
        let sig = sch.sign(&key.sk, &prog.dataset_id, &prog.labels[0], &msg, &mut rng);
        let double_sig = sch.combine(&prog.f, &vec![sig]);
        let mut id_pk_map = HashMap::new();
        id_pk_map.insert(id.to_vec(), key.pk.clone());
        let ver = sch.verify(&prog, &id_pk_map, &vec![Scalar::from(2 * 16)], &double_sig);

        assert!(ver)
    }

    #[test]
    fn schab19_sign_sum_verify() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = Sch2019::setup(20, 2, 1, &mut rng);
        let key_a = sch.keygen(&mut rng);
        let key_b = sch.keygen(&mut rng);
        let msg = vec![Scalar::from(16)];
        let msgb = vec![Scalar::from(256)];
        let id = b"signedbydieuwerh";
        let id_b = b"signedbyaclone";
        let did = b"testdatasetid";
        let label_a = Label {
            tag: 0,
            id: id.to_vec(),
        };
        let label_b = Label {
            tag: 1,
            id: id_b.to_vec(),
        };
        let prog = MLProg {
            dataset_id: did.to_vec(),
            labels: vec![label_a, label_b],
            f: vec![Scalar::from(1), Scalar::from(1)],
        };
        let sig = sch.sign(&key_a.sk, &prog.dataset_id, &prog.labels[0], &msg, &mut rng);
        let sigb = sch.sign(
            &key_b.sk,
            &prog.dataset_id,
            &prog.labels[1],
            &msgb,
            &mut rng,
        );
        let summed = sch.combine(&prog.f, &vec![sig, sigb]);
        let mut id_pk_map = HashMap::new();
        id_pk_map.insert(id.to_vec(), key_a.pk.clone());
        id_pk_map.insert(id_b.to_vec(), key_b.pk.clone());
        let ver = sch.verify(&prog, &id_pk_map, &vec![Scalar::from(16 + 256)], &summed);

        assert!(ver)
    }
    #[test]
    fn schab19_sign_linearly_combine_verify() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = Sch2019::setup(20, 2, 1, &mut rng);
        let key_a = sch.keygen(&mut rng);
        let key_b = sch.keygen(&mut rng);
        let msg = vec![Scalar::from(16)];
        let msgb = vec![Scalar::from(256)];
        let id = b"signedbydieuwerh";
        let id_b = b"signedbyaclone";
        let did = b"testdatasetid";
        let label_a = Label {
            tag: 0,
            id: id.to_vec(),
        };
        let label_b = Label {
            tag: 1,
            id: id_b.to_vec(),
        };
        let prog = MLProg {
            dataset_id: did.to_vec(),
            labels: vec![label_a, label_b],
            f: vec![Scalar::from(5), Scalar::from(123)],
        };
        let sig = sch.sign(&key_a.sk, &prog.dataset_id, &prog.labels[0], &msg, &mut rng);
        let sigb = sch.sign(
            &key_b.sk,
            &prog.dataset_id,
            &prog.labels[1],
            &msgb,
            &mut rng,
        );
        let summed = sch.combine(&prog.f, &vec![sig, sigb]);
        let mut id_pk_map = HashMap::new();
        id_pk_map.insert(id.to_vec(), key_a.pk.clone());
        id_pk_map.insert(id_b.to_vec(), key_b.pk.clone());
        let ver = sch.verify(
            &prog,
            &id_pk_map,
            &vec![Scalar::from((5 * 16) + (123 * 256))],
            &summed,
        );

        assert!(ver)
    }
}
