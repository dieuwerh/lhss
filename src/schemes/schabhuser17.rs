use bls12_381::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Scalar};
use ed::{Signer, Verifier};
use ff::Field;
use group::{Group, GroupEncoding};
use rand::Rng;
use rand_core::RngCore;
use rand_seeder::{Seeder, SipRng};

use crate::misc::types::File;

pub struct Sch17 {
    file_length: usize,
    pkt_length: usize,
    rs: Vec<G1Projective>,
    hs: Vec<G1Projective>,
}
#[derive(Debug, Clone)]
pub struct Sk {
    sk_sig: ed::SigningKey,
    k: [u8; 32],
}
pub type Vk = ed::VerifyingKey;

#[derive(Clone)]
pub struct Key {
    pub vk: Vk,
    pub sk: Sk,
}
pub type DId = Vec<u8>;
pub type Msg = Scalar;
pub type Vector = Vec<Msg>;
#[derive(Clone, Copy, Debug)]
pub struct Sig {
    sig_d: ed::Signature,
    z: G2Projective,
    lam: G1Projective,
}
#[derive(Debug)]
pub struct MLProg {
    f: Vec<Scalar>,
    did: DId,
}
impl MLProg {
    pub fn new(f: Vec<Scalar>, did: Vec<u8>) -> Self {
        Self { f, did }
    }
}

impl Sch17 {
    pub fn setup(file_length: usize, msg_length: usize, rng: &mut impl RngCore) -> Self {
        let mut rs = Vec::new();
        let mut hs = Vec::new();

        for _ in 0..file_length {
            let r = G1Projective::random(&mut *rng);
            rs.push(r);
        }
        for _ in 0..msg_length {
            let h = G1Projective::random(&mut *rng);
            hs.push(h);
        }

        Self {
            file_length,
            pkt_length: msg_length,
            rs,
            hs,
        }
    }
    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let mut k = [0u8; 32];
        rng.fill(&mut k);
        let mut ed_rng = rand::rngs::OsRng {};
        let ed_keypair = ed::SigningKey::generate(&mut ed_rng);
        let pk = ed_keypair.verifying_key();
        Key {
            sk: Sk {
                sk_sig: ed_keypair,
                k,
            },
            vk: pk,
        }
    }
    pub fn sign(&self, sk: &Sk, did: &[u8], tag: usize, m: &Vector) -> Sig {
        let z = Sch17::prf(&sk.k, did);
        let zz = G2Affine::generator() * z;
        let to_sign = [zz.to_bytes().as_ref(), did].concat();
        let sig_d = sk.sk_sig.try_sign(&to_sign).unwrap();
        let mut lam = self.rs[tag];
        for j in 0..self.pkt_length {
            let hcur = self.hs[j];
            let mcur = -m[j];
            let hcur = hcur * mcur;
            lam += hcur;
        }
        lam = lam * z;
        Sig { sig_d, z: zz, lam }
    }
    pub fn eval(&self, pk: &Vk, prog: &MLProg, sigs: &Vec<Sig>) -> Option<Sig> {
        // Verify if all signatures on the dataset are the same
        let sig_d = &sigs[0].sig_d;
        let z = &sigs[0].z;
        for i in 1..sigs.len() {
            let sigcur = &sigs[i];
            if sig_d != &sigcur.sig_d || z != &sigcur.z {
                return None;
            }
        }
        // Verify if signature checks out
        let msg = [z.to_bytes().as_ref(), &prog.did].concat();
        if pk.verify(&msg, &sig_d).is_err() {
            return None;
        }
        // Combine signatures and apply program coefficients
        let mut lam = sigs[0].lam * prog.f[0];
        for i in 1..sigs.len() {
            let lamcur = sigs[i].lam;
            let fcur = prog.f[i];
            lam = lam + (lamcur * fcur);
        }
        let sigres = Sig {
            sig_d: *sig_d,
            z: *z,
            lam,
        };
        Some(sigres)
    }
    pub fn verify(&self, pk: &Vk, prog: &MLProg, m: &Vector, sig: &Sig) -> bool {
        let did_msg = [sig.z.to_bytes().as_ref(), &prog.did].concat();
        if pk.verify(&did_msg, &sig.sig_d).is_err() {
            return false;
        }
        let mut r = self.rs[0] * prog.f[0];
        for i in 1..self.file_length {
            r = r + (self.rs[i] * prog.f[i]);
        }
        let mut mpart = self.hs[0] * (-m[0]);
        for i in 1..self.pkt_length {
            mpart = mpart + (self.hs[i] * (-m[i]));
        }
        let p1inp = G1Affine::from(r + mpart);

        let p1 = pairing(&p1inp, &G2Affine::from(sig.z));
        let p2 = pairing(&G1Affine::from(sig.lam), &G2Affine::generator());
        p1 == p2
    }
    fn prf(key: &[u8], id: &[u8]) -> Scalar {
        let input = [key, id].concat();
        let rng: SipRng = Seeder::from(input).make_rng();
        Scalar::random(rng)
    }
}

impl Sch17 {
    pub fn sign_file_single_key(&self, sk: &Sk, did: &[u8], file: &File<Scalar>) -> Vec<Sig> {
        file.iter()
            .enumerate()
            .map(|(i, packet)| self.sign(sk, did, i, packet))
            .collect()
    }
    pub fn sign_file_multiple_keys(
        &self,
        sks: &Vec<Sk>,
        did: &[u8],
        file: &File<Scalar>,
    ) -> Vec<Sig> {
        file.iter()
            .enumerate()
            .zip(sks)
            .map(|((i, packet), sk)| self.sign(sk, did, i, packet))
            .collect()
    }

    pub fn verify_file_single_vk(
        &self,
        vk: &Vk,
        mlprogs: &Vec<MLProg>,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .zip(mlprogs)
            .map(|((packet, sig), mlprog)| self.verify(vk, &mlprog, packet, sig))
            .collect()
    }

    pub fn verify_file_multiple_vks(
        &self,
        vks: &Vec<Vk>,
        mlprogs: &Vec<MLProg>,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        vks.iter()
            .zip(file)
            .zip(sigs)
            .zip(mlprogs)
            .map(|(((vk, packet), sig), mlprog)| self.verify(vk, &mlprog, packet, sig))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use rand_core::SeedableRng;

    use super::*;

    #[test]
    fn file_sign_ver() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let pl = 2;
        let fl = 2;

        let scheme = Sch17::setup(fl, pl, &mut rng);
        let key = scheme.keygen(&mut rng);
        let id = b"test id bytes";
        let f = vec![
            Scalar::from(0),
            Scalar::from(0),
            Scalar::from(0),
            Scalar::from(0),
        ];
        let mut prog = MLProg::new(f.clone(), id.to_vec());
        prog.f[0] = Scalar::from(1);
        let mut p2 = MLProg::new(f.clone(), id.to_vec());
        p2.f[1] = Scalar::from(1);
        let file: File<Scalar> = (0..fl)
            .map(|_| (0..pl).map(|_| Scalar::random(&mut rng)).collect())
            .collect();
        let sigs = scheme.sign_file_single_key(&key.sk, &id[..], &file);
        print!("{:#?}", sigs);
        let chekcs = scheme.verify_file_single_vk(&key.vk, &vec![prog, p2], &file, &sigs);
        print!("{:#?}", chekcs);
    }
}
