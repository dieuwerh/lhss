use bls12_381::{G1Projective, G2Projective, Gt, Scalar};
use ff::Field;
use group::Group;
use rand::RngCore;

use crate::misc::{bls_util::pair_proj, rand::GenRandomVec, types::File};

pub struct Li18 {
    g2: G2Projective,
    h: G1Projective,
    hs: Vec<G1Projective>,
    gs: Vec<G1Projective>,
    pub packet_length: usize,
    file_length: usize,
}

#[derive(Clone, Copy)]
pub struct Key {
    pub sk: Scalar,
    pub vk: G2Projective,
}
pub struct Sig {
    pub xs: Vec<G1Projective>,
    pub s: Scalar,
}
impl Li18 {
    /// Create instance of scheme Li18
    ///
    /// * `packet_length`: usize indicating the length of a packet (not augmented)
    /// * `file_length`: usize indicating the amount of packets in a file
    /// * `rng`: instance of RngCore, used to select generators of G1 and G2
    ///
    pub fn setup(packet_length: usize, file_length: usize, rng: &mut impl RngCore) -> Self {
        let g2 = G2Projective::generator();
        let h = G1Projective::random(&mut *rng);
        let hs: Vec<G1Projective> = rng.gen_rand_vec(file_length + 1);
        let gs: Vec<G1Projective> = rng.gen_rand_vec(packet_length);

        Self {
            g2,
            h,
            hs,
            gs,
            packet_length,
            file_length,
        }
    }

    /// Generate signing, verification key pair
    ///
    /// ## Arguments
    /// * `rng`: instance of RngCore, used to select random scalar
    ///
    /// ## Output
    /// * Keypair (sk vk)
    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let sk = Scalar::random(rng);
        let vk = self.g2 * sk;
        Key { sk, vk }
    }

    /// Signs a packet under a certain file id using the provided secret key
    ///
    /// ## Arguments
    /// * `sk` - The signing key
    /// * `pkt` - list of scalars, data to be signed. Should start with a unit vector indicating the packets index in the file.
    /// * `id` - The id of the file the packet belongs to: Scalar
    /// * `tag` - The index of this packet with regards to the file it belongs to
    /// * `rng` - Instance of rng core
    ///
    /// ## Output
    /// A signature on the provided packet
    pub fn sign(
        &self,
        sk: &Scalar,
        pkt: &Vec<Scalar>,
        id: &Scalar,
        tag: usize,
        rng: &mut impl RngCore,
    ) -> Sig {
        let s = Scalar::random(rng);

        let mut x = self.h * s;

        // Loop over unit vector in the packet (the first |file_length| items), zipped with the common parameters `hs`
        for (hj, unitj) in self.hs.iter().zip(&pkt[..self.file_length]) {
            if unitj != &Scalar::zero() {
                x += hj * unitj;
            }
        }
        for (gj, msgj) in self.gs.iter().zip(&pkt[self.file_length..]) {
            x += gj * msgj;
        }
        let inv_key = (sk + id).invert().unwrap();
        x *= inv_key;
        let mut xs = vec![G1Projective::identity(); self.file_length];
        xs[tag] = x;

        Sig { xs, s }
    }

    /// Combines signatures by raising them to provided coefficients and summing them together
    ///
    /// ## Arguments
    /// * `f` - list of coefficients: Vec<Scalar>
    /// * `signatures` - list of signatures
    ///
    /// ## Output
    pub fn combine(&self, f: &Vec<Scalar>, signatures: &Vec<Sig>) -> Sig {
        let mut sigres = Sig {
            xs: vec![G1Projective::identity(); signatures[0].xs.len()],
            s: Scalar::zero(),
        };
        for (fi, sig) in f.iter().zip(signatures) {
            sigres.s += fi * sig.s;
            for i in 0..sigres.xs.len() {
                sigres.xs[i] += sig.xs[i] * fi;
            }
        }
        sigres
    }

    /// Verifies a packet and signature with a public verification key
    ///
    /// ## Arguments
    /// * `pkt` - The packet that was signed: Vec<Scalar>
    /// * `sig` - The signature to verify
    /// * `id` - The id of the file that was signed
    /// * `vks` - the public keys to verify the signature with
    ///
    /// ## Output
    /// A boolean indicating if verification succeeded or not
    pub fn verify(
        &self,
        pkt: &Vec<Scalar>,
        sig: &Sig,
        id: &Scalar,
        vks: &Vec<G2Projective>,
    ) -> bool {
        let mut signautre_pairing = Gt::identity();
        for (x, vk) in sig.xs.iter().zip(vks) {
            let key = vk + (self.g2 * id);
            signautre_pairing += pair_proj(x, &key);
        }
        let mut reconstruct = self.h * sig.s;
        for (hj, unit_j) in self.hs.iter().zip(&pkt[..self.file_length]) {
            if unit_j != &Scalar::zero() {
                reconstruct += hj * unit_j;
            }
        }
        for (gj, msg_j) in self.gs.iter().zip(&pkt[self.file_length..]) {
            reconstruct += gj * msg_j;
        }
        let rec_pair = pair_proj(&reconstruct, &self.g2);
        signautre_pairing == rec_pair
    }

    /// Function to verify a signature which is not combined with any other signatures
    ///
    /// ## Argumnets
    /// * `pkt` - The packet that was signed.
    /// * `sig` - The signature on the packet to verify.
    /// * `id` -  The id of the file that was signed.
    /// * `tag` -  The tag of this packet, the index of the packet within the file.
    /// * `vk` -  The verification key to check the signature with.
    ///
    /// ## Returns
    /// bool inidcating whether the signature on the packet checks out.
    pub fn verify_non_combined(
        &self,
        pkt: &Vec<Scalar>,
        sig: &Sig,
        id: &Scalar,
        tag: usize,
        vk: &G2Projective,
    ) -> bool {
        let mut signautre_pairing = Gt::identity();
        let key = vk + (self.g2 * id);
        signautre_pairing += pair_proj(&sig.xs[tag], &key);
        let mut reconstruct = self.h * sig.s;
        for (hj, unit_j) in self.hs.iter().zip(&pkt[..self.file_length]) {
            if unit_j != &Scalar::zero() {
                reconstruct += hj * unit_j;
            }
        }
        for (gj, msg_j) in self.gs.iter().zip(&pkt[self.file_length..]) {
            reconstruct += gj * msg_j;
        }
        let rec_pair = pair_proj(&reconstruct, &self.g2);
        signautre_pairing == rec_pair
    }
}

impl Li18 {
    /// Function to sign all packets of a file using the same signing key.
    pub fn sign_file_single_key(
        &self,
        sk: &Scalar,
        file: &File<Scalar>,
        id: &Scalar,
        rng: &mut impl RngCore,
    ) -> Vec<Sig> {
        file.iter()
            .enumerate()
            .map(|(tag, packet)| self.sign(sk, packet, id, tag, rng))
            .collect()
    }

    /// Function to sign all packets of a file using the provided signing keys.
    /// Expects the number of signing keys to be equal to the lenght of the file.
    /// Will use the first signing key for the first packet in the file, and so on.
    pub fn sign_file_multiple_keys(
        &self,
        keys: &Vec<Key>,
        file: &File<Scalar>,
        id: &Scalar,
        rng: &mut impl RngCore,
    ) -> Vec<Sig> {
        keys.iter()
            .enumerate()
            .zip(file)
            .map(|((tag, key), pkt)| self.sign(&key.sk, pkt, id, tag, rng))
            .collect()
    }

    /// Function to verify all not yet combined signatures on the packets of a file.
    /// Assumes that all these signatures where created by the same signer.
    ///
    pub fn verify_file_single_vk(
        &self,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
        id: &Scalar,
        vk: &G2Projective,
    ) -> Vec<bool> {
        file.iter()
            .enumerate()
            .zip(sigs)
            .map(|((tag, packet), sig)| self.verify_non_combined(packet, sig, id, tag, vk))
            .collect()
    }

    /// Function to verify the not yet combined signatures on the packets of a file.
    /// Assumes there is a verification key and signature for each packet.
    pub fn verify_file_multiple_vks(
        &self,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
        file_id: &Scalar,
        vks: &Vec<G2Projective>,
    ) -> Vec<bool> {
        file.iter()
            .enumerate()
            .zip(sigs)
            .zip(vks)
            .map(|(((tag, packet), sig), vk)| {
                self.verify_non_combined(packet, sig, file_id, tag, vk)
            })
            .collect()
    }

    pub fn verify_file(
        &self,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
        ids: &Vec<Scalar>,
        vkss: &Vec<Vec<G2Projective>>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .zip(ids)
            .zip(vkss)
            .map(|(((pkt, sig), id), vks)| self.verify(pkt, sig, id, vks))
            .collect()
    }
}
