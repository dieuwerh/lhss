use std::fmt::Debug;

use crate::misc::bls_util::HashesToG1;
use crate::misc::types::Packet;
use crate::rand::Rng;
use crate::{ff::Field, misc::types::File};
use bls::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Gt, Scalar};
use rand::RngCore;

/***
 * Signature scheme by
 * @authors: Aranha and Pagnin
 * @title: 'The Simplest Multi-Key Linearly Homomorphic Signature Scheme'
 * @year: 2019
 * @model: Random Oracle Model,
 * @assumptions: co-CDH
 * @message: single value
 * @claims: better succinctness and performance
 * @remarks: based on boneh2009
 */

pub struct Ara2019 {
    g1: G1Affine,
    g2: G2Affine,
}
impl Ara2019 {
    pub fn setup() -> Self {
        // generate bilinear group
        // define ID, Tag and Message space
        Self {
            g1: G1Affine::generator(),
            g2: G2Affine::generator(),
        }
    }

    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let id: u8 = rng.gen();
        // println!("Generated id: {:?}", &id);
        let sk = Scalar::random(rng);
        let pk = self.g2 * sk;
        Key { id, sk, pk }
    }

    pub fn sign(&self, key: &Key, label: &Label, msg: &Msg) -> Sig {
        let label_hash = label.hash_to_g1();
        let g = (label_hash + (self.g1 * msg)) * key.sk;
        let m = msg;
        Sig {
            id: vec![key.id],
            g,
            m: vec![m.clone()],
        }
    }

    pub fn combine(&self, f: &Program, sigs: &Vec<Sig>) -> Sig {
        let mut g = sigs[0].g * f[0];
        let mut m = vec![sigs[0].m[0] * f[0]];
        let mut id = sigs[0].id.clone();
        if sigs.len() > 1 {
            for i in 1..f.len() {
                let f_cur = f[i];
                let s_cur = &sigs[i];
                g = g + (s_cur.g * f_cur);
                m.push(s_cur.m[0] * f_cur);
                if !id.contains(&s_cur.id[0]) {
                    id.push(s_cur.id[0]);
                }
            }
        };
        Sig { id, g, m }
    }

    pub fn verify(&self, p: &LabeledProgram, pks: &Vec<Pk>, m: &Msg, s: &Sig) -> bool {
        let v1 = m == &sum_msg(&s.m);
        if !v1 {
            return false;
        };

        let mut res = Gt::identity();

        for i in 0..p.f.len() {
            let m_cur = s.m[i];
            let f_cur = p.f[i];
            let pk_cur = pks[i];
            let l_cur = p.ls[i];

            res = res
                + pairing(
                    &G1Affine::from((self.g1 * m_cur) + (l_cur.hash_to_g1() * f_cur)),
                    &G2Affine::from(pk_cur),
                );
        }

        let v2 = pairing(&G1Affine::from(s.g), &self.g2) == res;
        if !v2 {
            return false;
        }
        return true;
    }
}

impl Ara2019 {
    pub fn sign_packet(&self, key: &Key, labels: &Vec<Label>, packet: &Packet<Scalar>) -> Vec<Sig> {
        labels
            .iter()
            .zip(packet)
            .map(|(label, msg)| self.sign(key, label, msg))
            .collect()
    }

    /// Function to sign a Packet, a list of messages, using multiple singing keys.
    /// Expects there to be a key and a label for each part of the packet.
    ///
    /// # Arguments
    /// * `keys` - List of signing keys to create signatures with.
    /// * `labels` - List of labels to sign the packet with
    /// * `packet` - Packet consisting of multiple messages, which is what gets signed.
    ///
    /// # Returns
    /// * A list of signatures on the messages contained in the packet.
    pub fn sign_packet_multiple_signers(
        &self,
        keys: &Vec<Key>,
        labels: &Vec<Label>,
        packet: &Packet<Scalar>,
    ) -> Vec<Sig> {
        labels
            .iter()
            .zip(packet)
            .zip(keys)
            .map(|((label, msg), key)| self.sign(key, label, msg))
            .collect()
    }

    pub fn sign_file_single_signer(
        &self,
        key: &Key,
        file_labels: &Vec<Label>,
        file: &File<Scalar>,
    ) -> Vec<Vec<Sig>> {
        file.iter()
            .map(|packet| self.sign_packet(key, file_labels, packet))
            .collect()
    }

    pub fn sign_file_multiple_signers(
        &self,
        keys: &Vec<Vec<Key>>,
        file_labels: &Vec<Label>,
        file: &File<Scalar>,
    ) -> Vec<Vec<Sig>> {
        keys.iter()
            .zip(file)
            .map(|(packet_keys, packet)| {
                self.sign_packet_multiple_signers(packet_keys, file_labels, packet)
            })
            .collect()
    }

    /// Verify the signatures made for the provided packet.
    pub fn verify_packet(
        &self,
        packet_program: &Vec<LabeledProgram>,
        vks: &Vec<Pk>,
        packet: &Packet<Scalar>,
        signatures: &Vec<Sig>,
    ) -> Vec<bool> {
        let vk_list: Vec<Vec<Pk>> = vks.iter().map(|vk| vec![vk.clone()]).collect();
        packet_program
            .iter()
            .zip(packet)
            .zip(signatures)
            .zip(vk_list)
            .map(|(((prog, msg), sig), vks)| self.verify(prog, &vks, msg, sig))
            .collect()
    }

    /// Verify the signatures made for the provided packet.
    ///
    /// Assumes all signatures where created with the same key, and can thus all
    /// be verifified with the same verification key.
    /// Constructs a vector consisting of only this verification key, before
    /// calling self.verify_packet(...)
    ///
    /// # Arguments
    /// * `packet_program` - A list of labeled programs under which which these packets were signed.
    /// * `vk` - The verification key to check the signatures with.
    /// * `packet` - The packet which items where signed.
    /// * `signatures` - The signatures on the packet to verify.
    ///
    /// # Returns
    /// * A list of bools indicating if the signatures verified
    ///
    pub fn verify_packet_single_vk(
        &self,
        packet_program: &Vec<LabeledProgram>,
        vk: &Pk,
        packet: &Packet<Scalar>,
        signatures: &Vec<Sig>,
    ) -> Vec<bool> {
        let vks = vec![vk.clone(); packet.len()];
        self.verify_packet(packet_program, &vks, packet, signatures)
    }

    /**
     * Each packet is singed using the same labeled program
     */
    pub fn verify_file(
        &self,
        packet_program: &Vec<LabeledProgram>,
        vks: &Vec<Vec<Pk>>,
        file: &File<Scalar>,
        file_signatures: &Vec<Vec<Sig>>,
    ) -> Vec<Vec<bool>> {
        file.iter()
            .zip(file_signatures)
            .zip(vks)
            .map(|((packet, sigs), packet_vks)| {
                self.verify_packet(packet_program, packet_vks, packet, sigs)
            })
            .collect()
    }

    /// Function to verify the signatures produced on a file.
    /// Since a file consists of n packets of m msgs, we expect
    /// signatures to be a n x m array of signatures produced on the
    /// messages of the file.
    /// This function is used for files that are signed using the same signing key.
    ///
    pub fn verify_file_single_vk(
        &self,
        packet_program: &Vec<LabeledProgram>,
        vk: &Pk,
        file: &File<Scalar>,
        file_signatures: &Vec<Vec<Sig>>,
    ) -> Vec<Vec<bool>> {
        file.iter()
            .zip(file_signatures)
            .map(|(packet, sigs)| self.verify_packet_single_vk(packet_program, vk, packet, sigs))
            .collect()
    }

    /**
     * We combine the messages at the same index of the various packets:
     * -------------------------------
     * | Sp1m1 | Sp1m2 | ... | Sp1mn |
     * -------------------------------
     * | Sp2m1 | Sp2m2 | ... | Sp2mn |
     * -------------------------------
     * ....
     * -------------------------------
     * | Spxm1 | Spxm2 | ... | Spxmn |
     * -------------------------------
     * Will become
     * -------------------------------
     * | Spcombinem1 | Spcombinem2 | ... | Spcombinemn |
     * -------------------------------
     * The results will thus be a vector of signatures.
     *
     * f is a program with coefficients corresponding to the number of packets in a file (file length)
     */
    pub fn combine_file_signatures(&self, f: &Program, sigs: &Vec<Vec<Sig>>) -> Vec<Sig> {
        let mut res = Vec::new();

        for message_index in 0..sigs[0].len() {
            let cur_sigs: Vec<Sig> = sigs
                .iter()
                .map(|packetsigs| packetsigs[message_index].clone())
                .collect();
            let combined = self.combine(f, &cur_sigs);
            res.push(combined);
        }
        res
    }
}

fn sum_msg(msgs: &Vec<Msg>) -> Msg {
    let mut sum = Scalar::zero();

    for msg in msgs {
        sum = sum + msg;
    }

    sum
}

type Id = u8;
type Tag = u8;
type Sk = Scalar;
pub type Pk = G2Projective;
type Msg = Scalar;

#[derive(Hash, Clone, Copy)]
pub struct Label {
    id: Id,
    tag: Tag,
}

impl Label {
    pub fn new(id: &Id, tag: &Tag) -> Self {
        Self {
            id: id.clone(),
            tag: tag.clone(),
        }
    }
    pub fn to_num(&self) -> u16 {
        self.id as u16 + self.tag as u16
    }
    pub fn to_num_string(&self) -> String {
        self.to_num().to_string()
    }
    fn hash_to_g1(&self) -> G1Projective {
        let to_hash = [self.id, self.tag];
        (&to_hash[..]).hash_g1()
    }
}
impl Debug for Label {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Label {{id: {}, tag: {} }}", &self.id, &self.tag)
    }
}

#[derive(Clone, Copy)]
pub struct Key {
    pub id: Id,
    pub sk: Sk,
    pub pk: Pk,
}

#[derive(Clone)]
pub struct Sig {
    id: Vec<Id>,
    g: G1Projective,
    m: Vec<Msg>,
}
impl Debug for Sig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Sig{{ id: {:?}, m: {:?}, \ng: {:?} }}",
            &self.id,
            &self.m.short_repr(),
            &self.g
        )
    }
}

trait ShortRep {
    fn short_repr(&self) -> String;
}
impl ShortRep for Scalar {
    fn short_repr(&self) -> String {
        let tmp = self.to_bytes();
        let mut res = String::from("0x");
        let mut iter = tmp.iter().rev();
        let mut b = iter.next();
        while b == Some(&0x00) {
            b = iter.next();
        }
        for &b in iter {
            res = format!("{}{}", res, b);
        }
        res
    }
}
impl ShortRep for Vec<Scalar> {
    fn short_repr(&self) -> String {
        let mut res = Vec::new();
        for scal in self {
            res.push(scal.short_repr());
        }
        format!("[{}]", res.join(", "))
    }
}
impl ShortRep for Vec<Vec<Scalar>> {
    fn short_repr(&self) -> String {
        let mut res = Vec::new();
        for list in self {
            res.push(list.short_repr());
        }
        format!("[{}]", res.join(",\n "))
    }
}

pub type Coefficient = Scalar;
pub type Program = Vec<Coefficient>;
pub fn sum_program(length: usize) -> Program {
    let mut prog = Program::new();
    for _ in 0..length {
        prog.push(Scalar::from(1));
    }
    prog
}

pub struct LabeledProgram {
    f: Program,
    ls: Vec<Label>,
}

impl LabeledProgram {
    pub fn new(f: &Program, ls: &Vec<Label>) -> Self {
        Self {
            f: f.clone(),
            ls: ls.clone(),
        }
    }
}
