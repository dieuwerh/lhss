use std::collections::HashMap;

use bls12_381::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Gt, Scalar};
use ed::{Signer, Verifier};
use ff::Field;
use group::{Group, GroupEncoding};
use rand::{Rng, RngCore};
use rand_seeder::{Seeder, SipRng};

use crate::misc::{bls_util::pair_proj, types::File};

pub struct Schab2018 {
    pub k: usize,
    n: usize,
    t: usize,
    hs: Vec<G1Projective>,
    g1: G1Affine,
    g2: G2Affine,
    gt: Gt,
}
impl Schab2018 {
    pub fn setup(
        id_space_size: usize,
        tag_space_size: usize,
        msg_length: usize,
        rng: &mut impl RngCore,
    ) -> Self {
        let mut hs = Vec::new();
        for _ in 0..msg_length {
            hs.push(G1Projective::random(&mut *rng));
        }
        let gt = pairing(&G1Affine::generator(), &G2Affine::generator());
        Self {
            k: id_space_size,
            n: tag_space_size,
            t: msg_length,
            hs,
            g1: G1Affine::generator(),
            g2: G2Affine::generator(),
            gt,
        }
    }
    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let mut k = [0u8; 32];
        rng.fill(&mut k);
        let mut ed_rng = rand::rngs::OsRng {};
        let ed_keypair = ed::SigningKey::generate(&mut ed_rng);
        let mut xs = Vec::new();
        let mut hs = Vec::new();
        for _ in 0..self.n {
            let x = Scalar::random(&mut *rng);
            let h = self.gt * x;
            xs.push(x);
            hs.push(h);
        }
        let y = Scalar::random(&mut *rng);
        let sk = SK {
            k,
            sig_key: ed_keypair,
            xs,
            y,
        };
        let pk = PK {
            sig_pk: sk.sig_key.verifying_key(),
            hs,
            y: self.g2 * sk.y,
        };
        Key { sk, pk }
    }
    pub fn sign(
        &self,
        sk: &SK,
        did: &[u8],
        l: &Label,
        msg: &Vector,
        rng: &mut impl RngCore,
    ) -> Sig {
        let z = Schab2018::prf(&sk.k, did);
        let big_z = self.g2 * z;
        let to_sign = [big_z.to_bytes().as_ref(), did].concat();
        let sig_d = sk.sig_key.try_sign(&to_sign).unwrap();
        let r = Scalar::random(&mut *rng);
        let s = Scalar::random(&mut *rng);
        let mut a = self.g1 * (sk.xs[l.t] + r);
        let mut c = self.g1 * s;
        for i in 0..self.t {
            a = a + (self.hs[i] * (sk.y * msg[i]));
            c = c + (self.hs[i] * msg[i]);
        }
        a = a * (z.invert().unwrap());

        let r = self.g1 * (r - (sk.y * s));
        let s = self.g2 * (-s);
        let lam = Lam {
            id: l.id.clone(),
            sig_d,
            z: big_z,
            a,
            c,
        };
        Sig {
            lams: vec![lam],
            r,
            s,
        }
    }

    pub fn combine(&self, f: &Coefs, sigs: &Vec<Sig>) -> Sig {
        let mut r = G1Projective::identity();
        let mut s = G2Projective::identity();
        let mut lam_ids: HashMap<u8, Lam> = HashMap::new();
        for i in 0..self.n {
            let sig = &sigs[i];
            r = r + (sig.r * f[i]);
            s = s + (sig.s * f[i]);
            for lam in sig.lams.iter() {
                let id_entry = lam_ids.entry(lam.id).or_insert(lam.empty_clone());
                id_entry.a += lam.a * f[i];
                id_entry.c += lam.c * f[i];
            }
        }
        Sig {
            lams: (lam_ids.into_values().collect()),
            r,
            s,
        }
    }
    pub fn verify(&self, prog: &MLProg, vks: &HashMap<u8, PK>, msg: &Vector, sig: &Sig) -> bool {
        for lam in sig.lams.iter() {
            let did_msg = [lam.z.to_bytes().as_ref(), &prog.did].concat();
            if vks[&lam.id].sig_pk.verify(&did_msg, &lam.sig_d).is_err() {
                println!("Dataset signature fails");
                return false;
            }
        }

        let mut p1 = Gt::identity();
        let mut p2 = Gt::identity();
        let mut c_tot = G1Projective::identity();
        for lam in sig.lams.iter() {
            p1 += pair_proj(&lam.a, &lam.z);
            let y = &vks[&lam.id].y;
            p2 += pair_proj(&lam.c, &y);
            c_tot += lam.c;
        }
        for (i, label) in prog.labels.iter().enumerate() {
            p2 += vks[&label.id].hs[label.t] * prog.f[i];
        }
        p2 += pairing(&G1Affine::from(sig.r), &self.g2);

        let mut p3 = pairing(&self.g1, &G2Affine::from(sig.s));
        p3 += pairing(&G1Affine::from(c_tot), &self.g2);

        let mut h_total = G1Projective::identity();
        for i in 0..self.t {
            h_total += self.hs[i] * msg[i];
        }
        let p4 = pairing(&G1Affine::from(h_total), &self.g2);
        let ver1 = p1 == p2;
        let ver2 = p3 == p4;

        ver1 && ver2
    }

    fn prf(key: &[u8], id: &[u8]) -> Scalar {
        let input = [key, id].concat();
        let mut rng: SipRng = Seeder::from(input).make_rng();
        Scalar::random(&mut rng)
    }
}

impl Schab2018 {
    pub fn sign_file_single_key(
        &self,
        sk: &SK,
        did: &[u8],
        labels: &Vec<Label>,
        file: &File<Scalar>,
        rng: &mut impl RngCore,
    ) -> Vec<Sig> {
        labels
            .iter()
            .zip(file)
            .map(|(l, msg)| self.sign(sk, did, l, msg, rng))
            .collect()
    }
    pub fn sign_file_multiple_keys(
        &self,
        sks: &Vec<SK>,
        did: &[u8],
        labels: &Vec<Label>,
        file: &File<Scalar>,
        rng: &mut impl RngCore,
    ) -> Vec<Sig> {
        sks.iter()
            .zip(labels)
            .zip(file)
            .map(|((sk, l), msg)| self.sign(sk, did, l, msg, rng))
            .collect()
    }

    pub fn verify_file(
        &self,
        vks: &VerificationKeys,
        progs: &Vec<MLProg>,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .zip(progs)
            .map(|((packet, sig), prog)| self.verify(prog, vks, packet, sig))
            .collect()
    }
}

#[derive(Clone)]
pub struct SK {
    k: [u8; 32],
    sig_key: ed25519_dalek::SigningKey,
    xs: Vec<Scalar>,
    y: Scalar,
}
#[derive(Clone)]
pub struct PK {
    sig_pk: ed25519_dalek::VerifyingKey,
    hs: Vec<Gt>,
    y: G2Projective,
}

pub type VerificationKeys = HashMap<u8, PK>;

#[derive(Clone)]
pub struct Key {
    pub sk: SK,
    pub pk: PK,
}
pub type DId = Vec<u8>;
pub type Msg = Scalar;
pub type Vector = Vec<Msg>;
#[derive(Clone)]
pub struct Label {
    pub t: usize,
    pub id: u8,
}
impl Label {
    pub fn new(tag: usize, id: u8) -> Self {
        Self { t: tag, id }
    }
}
#[derive(Clone, Copy, Debug)]
pub struct Lam {
    id: u8,
    sig_d: ed::Signature,
    z: G2Projective,
    a: G1Projective,
    c: G1Projective,
}
impl Lam {
    fn empty_clone(&self) -> Self {
        Lam {
            id: self.id,
            sig_d: self.sig_d,
            z: self.z,
            a: G1Projective::identity(),
            c: G1Projective::identity(),
        }
    }
}
#[derive(Debug)]
pub struct Sig {
    lams: Vec<Lam>,
    r: G1Projective,
    s: G2Projective,
}
pub type Coefs = Vec<Scalar>;
#[derive(Clone)]
pub struct MLProg {
    pub did: DId,
    pub labels: Vec<Label>,
    pub f: Coefs,
}
impl MLProg {
    pub fn new(did: &[u8], labels: Vec<Label>, f: Vec<Scalar>) -> Self {
        Self {
            did: did.to_vec(),
            labels,
            f,
        }
    }
}
