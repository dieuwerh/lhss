use crate::group::GroupEncoding;
use crate::misc::bls_util::*;
use crate::misc::num_traits::AddVecs;
use crate::misc::types::File;
use crate::{ff::Field, misc::num_traits::ScaleVec};
use bls12_381::{G1Projective, G2Projective, Scalar};
use rand::RngCore;

/// Title: Secure Data Delivery with Identity-based Linearly Homomorphic Network Coding Signature Scheme in IoT
///
/// Authors: Yumei Li, Futai Zhang, Xin Liu
///
/// Assumption: co-CDH
pub struct Li2020 {
    g: G1Projective,
    h: G2Projective,
    s: Scalar,
    mpk: G2Projective,
}

impl Li2020 {
    pub fn setup(rng: &mut impl RngCore) -> Self {
        let g = G1Projective::generator();
        let h = G2Projective::generator();
        let s = Scalar::random(rng);
        let mpk = h * s;
        Li2020 { g, h, s, mpk }
    }
    pub fn keyext(&self, id_bytes: &[u8], rng: &mut impl RngCore) -> Sk {
        let r = Scalar::random(rng);
        let big_r = self.h * r;
        let x = r + (self.s * Li2020::h0(id_bytes, &big_r));
        Sk { x, big_r }
    }
    pub fn verify_private_key(&self, id_bytes: &[u8], sk: &Sk) -> bool {
        let pair1 = pair_proj(&(self.g * sk.x), &self.h);
        let pair2 = pair_proj(
            &self.g,
            &(sk.big_r + (self.mpk * Li2020::h0(&id_bytes, &sk.big_r))),
        );

        pair1 == pair2
    }
    pub fn sign_data(
        &self,
        sk: &Sk,
        id_bytes: &[u8],
        file_id_bytes: &[u8],
        msgs_vector: &Vec<Vec<Scalar>>,
    ) -> (Vec<Sig>, Tag) {
        let num_msgs = msgs_vector.len();
        let msg_length = msgs_vector[0].len();
        let mut sigs = Vec::new();
        for i in 0..num_msgs {
            let mut augment_part = vec![Scalar::zero(); num_msgs];
            augment_part[i] = Scalar::one();
            let augmented_msg = [msgs_vector[i].clone(), augment_part].concat();
            let sig = self.sign(sk, id_bytes, file_id_bytes, &augmented_msg, msg_length);
            sigs.push(sig);
        }
        (
            sigs,
            Tag {
                file_id_bytes: file_id_bytes.to_vec(),
                big_r: sk.big_r.clone(),
            },
        )
    }
    pub fn sign(
        &self,
        sk: &Sk,
        id_bytes: &[u8],
        file_id_bytes: &[u8],
        augmented_vector: &Vec<Scalar>,
        msg_length: usize,
    ) -> Sig {
        let mut sig = G1Projective::identity();
        for i in msg_length..augmented_vector.len() {
            if augmented_vector[i] == Scalar::zero() {
                continue;
            }
            sig += Li2020::h1(&id_bytes, i) * augmented_vector[i];
        }
        let mut msg_exp = Scalar::zero();
        for j in 0..msg_length {
            msg_exp += Li2020::h2(id_bytes, j, file_id_bytes, &sk.big_r) * augmented_vector[j];
        }
        let msg_part = self.g * msg_exp;
        sig += msg_part;
        sig *= sk.x;
        sig
    }
    pub fn verify(
        &self,
        id_bytes: &[u8],
        tag: &Tag,
        msg_length: usize,
        augmented_vector: &Vec<Scalar>,
        sig: &Sig,
    ) -> bool {
        let signature_pair = pair_proj(&sig, &self.h);
        let mut augment_hashes = G1Projective::identity();
        for i in msg_length..augmented_vector.len() {
            let augment_part = augmented_vector[i];
            if augment_part != Scalar::zero() {
                augment_hashes += Li2020::h1(id_bytes, i) * augment_part;
            }
        }
        let mut msg_exp = Scalar::zero();
        for j in 0..msg_length {
            msg_exp +=
                Li2020::h2(id_bytes, j, &tag.file_id_bytes, &tag.big_r) * augmented_vector[j];
        }
        let inpg1 = augment_hashes + (self.g * msg_exp);

        let reconstructed_pair = pair_proj(
            &inpg1,
            &(tag.big_r + (self.mpk * Li2020::h0(id_bytes, &tag.big_r))),
        );
        signature_pair == reconstructed_pair
    }
    pub fn combine(&self, f: &Vec<Scalar>, signatures: &Vec<Sig>) -> Sig {
        let mut sig_result = G1Projective::identity();
        for i in 0..f.len() {
            sig_result += signatures[i] * f[i];
        }
        sig_result
    }
    pub fn combine_and_calc_msg(
        &self,
        f: &Vec<Scalar>,
        augmented_vectors: &Vec<Vec<Scalar>>,
        signatures: &Vec<Sig>,
    ) -> (Vec<Scalar>, Sig) {
        let mut msg_result = vec![Scalar::zero(); augmented_vectors[0].len()];
        let mut sig_result = G1Projective::identity();
        for i in 0..f.len() {
            let scaled = augmented_vectors[i].scale(&f[i]);
            msg_result.add_vec_mut(&scaled);
            sig_result += signatures[i] * f[i];
        }
        (msg_result, sig_result)
    }
}

impl Li2020 {
    /**
     * Sign an entire file which is already augmented with a unit vector:
     * [msg0, ..., msg_msg_length, unit0, ..., unit_file_length]
     */
    pub fn sign_file_single_key(
        &self,
        sk: &Sk,
        signer_id_bytes: &[u8],
        file_id_bytes: &[u8],
        file: &File<Scalar>,
        msg_length: usize,
    ) -> Vec<Sig> {
        file.iter()
            .map(|packet| self.sign(sk, signer_id_bytes, file_id_bytes, packet, msg_length))
            .collect()
    }
    pub fn sign_file_multiple_keys(
        &self,
        sks: &Vec<Sk>,
        signer_ids_bytes: &Vec<Vec<u8>>,
        file_id_bytes: &[u8],
        file: &File<Scalar>,
        msg_length: usize,
    ) -> Vec<Sig> {
        sks.iter()
            .zip(signer_ids_bytes)
            .zip(file)
            .map(|((sk, id_bytes), augmented_vector)| {
                self.sign(sk, id_bytes, file_id_bytes, augmented_vector, msg_length)
            })
            .collect()
    }

    pub fn verify_file_single_vk(
        &self,
        signer_id: &[u8],
        file_tag: &Tag,
        file: &File<Scalar>,
        signatures: &Vec<Sig>,
        msg_length: usize,
    ) -> Vec<bool> {
        file.iter()
            .zip(signatures)
            .map(|(packet, sig)| self.verify(signer_id, file_tag, msg_length, packet, sig))
            .collect()
    }

    pub fn verify_file_multiple_vks(
        &self,
        signer_ids: &Vec<Vec<u8>>,
        file_tags: &Vec<Tag>,
        file: &File<Scalar>,
        signatures: &Vec<Sig>,
        msg_length: usize,
    ) -> Vec<bool> {
        signer_ids
            .iter()
            .zip(file_tags)
            .zip(file)
            .zip(signatures)
            .map(|(((signer_id, file_tag), packet), sig)| {
                self.verify(&signer_id, file_tag, msg_length, packet, sig)
            })
            .collect()
    }
}

impl Li2020 {
    fn h0(input_bytes: &[u8], g2_element: &G2Projective) -> Scalar {
        let bytes_to_hash = [input_bytes, g2_element.to_bytes().as_ref()].concat();
        bytes_to_hash.hash_scalar()
    }
    fn h1(input_bytes: &[u8], index: usize) -> G1Projective {
        let bytes_to_hash = [input_bytes, &[index as u8]].concat();
        bytes_to_hash.hash_g1()
    }
    fn h2(
        input_bytes: &[u8],
        index: usize,
        security_bytes: &[u8],
        g2_element: &G2Projective,
    ) -> Scalar {
        let bytes_to_hash = [
            input_bytes,
            &index.to_be_bytes(),
            security_bytes,
            g2_element.to_bytes().as_ref(),
        ]
        .concat();
        bytes_to_hash.hash_scalar()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Sk {
    x: Scalar,
    pub big_r: G2Projective,
}
pub type Sig = G1Projective;

pub struct Tag {
    pub file_id_bytes: Vec<u8>,
    pub big_r: G2Projective,
}
impl Tag {
    pub fn new(file_id: &[u8], bigr: &G2Projective) -> Self {
        Tag {
            file_id_bytes: file_id.to_vec(),
            big_r: bigr.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::rand::SeedableRng;

    #[test]
    fn li20_keyext_verify() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let sch = Li2020::setup(&mut rng);
        let id = b"thisisdieuwersid";
        let key = sch.keyext(&id[..], &mut rng);
        let ver = sch.verify_private_key(&id[..], &key);
        assert!(ver)
    }

    #[test]
    fn li20_sign_verify() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let sch = Li2020::setup(&mut rng);
        let id = b"thisisdieuwersid";
        let sk = sch.keyext(&id[..], &mut rng);

        let msgs = vec![vec![Scalar::from(123), Scalar::from(456)]];

        let file_id_bytes = b"this is the filename";
        let (sigs, tag) = sch.sign_data(&sk, &id[..], &file_id_bytes[..], &msgs);

        let ver = sch.verify(
            &id[..],
            &tag,
            2,
            &vec![Scalar::from(123), Scalar::from(456), Scalar::from(1)],
            &sigs[0],
        );

        assert!(ver)
    }

    #[test]
    fn li20_sign_double_verify() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let sch = Li2020::setup(&mut rng);
        let id = b"thisisdieuwersid";
        let sk = sch.keyext(&id[..], &mut rng);

        let msgs = vec![vec![Scalar::from(123), Scalar::from(456)]];
        let msgs_augmented = vec![vec![Scalar::from(123), Scalar::from(456), Scalar::from(1)]];

        let file_id_bytes = b"this is the filename";
        let (sigs, tag) = sch.sign_data(&sk, &id[..], &file_id_bytes[..], &msgs);

        let (doubled_msg, _) =
            sch.combine_and_calc_msg(&vec![Scalar::from(2)], &msgs_augmented, &sigs);
        let doubled_sig = sch.combine(&vec![Scalar::from(2)], &sigs);
        let ver = sch.verify(&id[..], &tag, 2, &doubled_msg, &doubled_sig);

        assert!(ver)
    }

    #[test]
    fn li20_combine_3_msgs() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let sch = Li2020::setup(&mut rng);
        let id = b"thisisdieuwersid";
        let sk = sch.keyext(&id[..], &mut rng);

        let msgs = vec![
            vec![Scalar::from(123), Scalar::from(456)],
            vec![Scalar::from(3), Scalar::from(4)],
            vec![Scalar::from(6), Scalar::from(7)],
        ];
        let msgs_augmented = vec![
            vec![
                Scalar::from(123),
                Scalar::from(456),
                Scalar::from(1),
                Scalar::from(0),
                Scalar::from(0),
            ],
            vec![
                Scalar::from(3),
                Scalar::from(4),
                Scalar::from(0),
                Scalar::from(1),
                Scalar::from(0),
            ],
            vec![
                Scalar::from(6),
                Scalar::from(7),
                Scalar::from(0),
                Scalar::from(0),
                Scalar::from(1),
            ],
        ];

        let file_id_bytes = b"this is the filename";
        let (sigs, tag) = sch.sign_data(&sk, &id[..], &file_id_bytes[..], &msgs);

        let (combined_msg, _) = sch.combine_and_calc_msg(
            &vec![Scalar::from(2), Scalar::from(5)],
            &msgs_augmented,
            &sigs,
        );
        let combined_sig = sch.combine(&vec![Scalar::from(2), Scalar::from(5)], &sigs);
        let ver = sch.verify(&id[..], &tag, 2, &combined_msg, &combined_sig);

        assert!(ver)
    }
}
