use crate::{
    ff::Field,
    misc::{rand::GenRandomVec, types::File},
};
use bls::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Scalar};
use rand::RngCore;

/***
 * Signature scheme by
 * @authors: Catalano, Fiore, Warinschi
 * @title: 'Efficient Network Coding Signatures in the Standard Model'
 * @year: 2012
 * @model: Standard model
 * @assumptions: q-Strong Diffie Hellman
 * @message: Vectors
 * @claims: 'in the same efficiency class as previous solutions based on ROM'
 * @remarks:
 * @PROBLEM:
 *
 */

/// Instance of Catalano 2012 scheme
///
/// * `h` - Generator of G2 used for public keys
/// * `m` - Usize indicating the number of packets in a file
/// * `n` - Usize indicating the number of items in a packet
pub struct Cat12 {
    h: G2Affine,
    m: usize,
    n: usize,
}

impl Cat12 {
    /// Create instance of scheme
    ///
    /// ## Arguments
    /// * `packet_length`: usize indicating number of items in a packet
    /// * `file_length`: usize indicating how many packets are in a file
    ///
    /// ## Operations
    /// * select generator h <- G2
    pub fn setup(packet_length: usize, file_length: usize) -> Self {
        Cat12 {
            h: G2Affine::generator(),
            m: file_length,
            n: packet_length,
        }
    }

    /// Generate a key pair
    ///
    /// ## Arguments
    /// * `rng` - mutable instance of RngCore, used to select a random scalar for the secret key and random generators of g1 for the public key
    ///
    /// ## Operations
    /// * select signing key sk <-$ Zq*
    /// * compute verification key z = h ^ sk  
    /// * choose random elements of G1: h,h1,...hm , g1,..,gn <-$ G1
    ///
    /// ## Output
    /// * Keypair consisting of signing key sk and verification key vk
    pub fn keygen(&self, rng: &mut impl RngCore) -> Key {
        let sk = Scalar::random(&mut *rng);
        let z = self.h * sk;
        let hs: Vec<G1Projective> = rng.gen_rand_vec(self.m + 1);
        let gs: Vec<G1Projective> = rng.gen_rand_vec(self.n);
        Key {
            sk,
            vk: Vk { z, hs, gs },
        }
    }

    /// Signs a packet under a certain file id using the provided secret key
    /// A packet is structured as the concatenation of [unit vector] [data vector],
    /// where the unit vector indicates which part of the file the current packet is.
    /// E.g. [1 0 0 0 123, 543, 562] is the first of 4 packets in file.
    ///
    /// ## Arguments
    /// * `k` - The signers key pair
    /// * `fid` - The id of the file the packet belongs to: Scalar
    /// * `packet` - list of scalars, data to be signed
    ///
    /// ## Operations
    /// * select random scalar s
    /// * compute X = ( h^s *
    /// ∏ i=1..m (
    ///     h_i^(packet\[i])
    /// ) * ∏ j=1..n (
    ///     g_j ^(packet\[m+j]
    ///  )
    ///  )^ (1 / sk + fid)
    /// * sig = (X,s)
    ///
    /// ## Output
    /// A signature on the provided packet
    pub fn sign(&self, k: &Key, fid: &Scalar, packet: &Vec<Scalar>, rng: &mut impl RngCore) -> Sig {
        let s = Scalar::random(rng);
        let mut x = k.vk.hs[0] * s;

        for i in 0..self.m {
            if packet[i] == Scalar::one() {
                let cur = k.vk.hs[i + 1] * packet[i];
                x = x + cur
            }
        }
        for j in self.m..self.m + self.n {
            let g_index = j - self.m;
            let cur = k.vk.gs[g_index] * packet[j];
            x += cur;
        }
        let inverted = (k.sk + fid).invert().unwrap();
        x = x * inverted;
        Sig { x, s }
    }

    /// Verifies a packet and signature with a  public key
    ///
    /// ## Arguments
    /// * `pk` - the public key to verify the signature with
    /// * `id_bytes` - The id of the file that was signed
    /// * `packet` - The packet that was signed: Vec<Scalar>
    /// * `sig` - The signature to verify: G1
    ///
    /// ## Operations
    /// * p1 = e(X, vk)
    /// * p2 = e(h^s *
    /// ∏ i=1..m (
    ///     h_i^(packet\[i])
    /// ) * ∏ j=1..n (
    ///     g_j ^(packet\[m+j]
    ///  )  , h)
    /// * p1 =? p2
    ///
    /// ## Output
    /// A boolean indicating if verification succeeded or not
    pub fn verify(&self, vk: &Vk, fid: &Scalar, packet: &Vec<Scalar>, sig: &Sig) -> bool {
        let pair1 = pairing(
            &G1Affine::from(sig.x),
            &G2Affine::from(vk.z + (self.h * fid)),
        );
        let mut msg_part = vk.hs[0] * sig.s;

        let m = self.m;
        let n = self.n;
        for i in 0..m {
            if packet[i] != Scalar::zero() {
                let cur = vk.hs[i + 1] * packet[i];
                msg_part += cur;
            }
        }
        for j in m..m + n {
            let g_index = j - m;
            let cur = vk.gs[g_index] * packet[j];
            msg_part += cur;
        }

        let pair2 = pairing(&G1Affine::from(msg_part), &self.h);
        pair1 == pair2
    }

    /// Combines signatures by raising them to provided coefficients and summing them together
    ///
    /// ## Arguments
    /// * `f` - list of coefficients: Vec<Scalar>
    /// * `sigs` - list of signatures: Vec<G1>
    ///
    /// ## Operations
    /// * `sig <- ∏i=1..m  sig_i ^ f_i
    ///
    /// ## Output
    /// * A signature
    pub fn combine(&self, f: &Vec<Scalar>, sigs: &Vec<Sig>) -> Sig {
        let mut x_res = G1Projective::identity();
        let mut s_res = Scalar::zero();
        for i in 0..sigs.len() {
            x_res = x_res + (sigs[i].x * f[i]);
            s_res = s_res + (sigs[i].s * f[i]);
        }

        Sig { x: x_res, s: s_res }
    }
}

impl Cat12 {
    pub fn sign_file_single_key(
        &self,
        k: &Key,
        file_id: &Scalar,
        file: &File<Scalar>,
        rng: &mut impl RngCore,
    ) -> Vec<Sig> {
        file.iter()
            .map(|packet| self.sign(k, file_id, packet, rng))
            .collect()
    }

    pub fn sign_file_multiple_keys(
        &self,
        ks: &Vec<Key>,
        file_id: &Scalar,
        file: &File<Scalar>,
        rng: &mut impl RngCore,
    ) -> Vec<Sig> {
        ks.iter()
            .zip(file)
            .map(|(k, packet)| self.sign(k, file_id, packet, rng))
            .collect()
    }

    pub fn verify_file_single_vk(
        &self,
        vk: &Vk,
        fid: &Scalar,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .map(|(packet, sig)| self.verify(vk, fid, packet, sig))
            .collect()
    }

    pub fn verify_file_multiple_vks(
        &self,
        vks: &Vec<Vk>,
        fid: &Scalar,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .zip(vks)
            .map(|((packet, sig), vk)| self.verify(vk, fid, packet, sig))
            .collect()
    }
}

/// A privat signing key: scalar
pub type Sk = Scalar;

/// A public verification key. It contains
/// z: an element of g2,
/// hs: generators of g1 used for the unit vector part of an augmented packet,
/// gs: generators of g1 used for the data part of an augmented packet
#[derive(Clone)]
pub struct Vk {
    z: G2Projective,
    hs: Vec<G1Projective>,
    gs: Vec<G1Projective>,
}

/// Container for key pairs
///
/// contains the private signing key and the public verification key
#[derive(Clone)]
pub struct Key {
    pub sk: Sk,
    pub vk: Vk,
}

/// A signature
///
/// It contains
/// * x: an element of g1
/// * s: a randomly chosen scalar
pub struct Sig {
    x: G1Projective,
    s: Scalar,
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;

    use super::*;
    use crate::misc::num_traits::*;

    #[test]
    fn catalano12_sign_verify_basic() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let msg_length = 1;
        let sch = Cat12::setup(msg_length, 1);
        let key = sch.keygen(&mut rng);
        let msg = vec![Scalar::random(&mut rng); msg_length];
        let msg = [vec![Scalar::from(1)], msg].concat();
        let id = Scalar::from(123);
        let sig = sch.sign(&key, &id, &msg, &mut rng);
        let ver = sch.verify(&key.vk, &id, &msg, &sig);
        assert!(ver);
    }
    #[test]
    fn catalano12_sign_verify_msglegnth_5() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let msg_length = 5;
        let sch = Cat12::setup(msg_length, 1);
        let key = sch.keygen(&mut rng);

        let msg = vec![Scalar::random(&mut rng); msg_length];
        let msg = [vec![Scalar::from(1)], msg].concat();
        let id = Scalar::from(123);
        let sig = sch.sign(&key, &id, &msg, &mut rng);
        let ver = sch.verify(&key.vk, &id, &msg, &sig);
        assert!(ver);
    }
    #[test]
    fn catalano12_sign_double_verify() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let msg_length = 1;
        let sch = Cat12::setup(msg_length, 1);
        let key = sch.keygen(&mut rng);

        let msg = vec![Scalar::random(&mut rng); msg_length];
        let msg = [vec![Scalar::from(1)], msg].concat();
        let id = Scalar::from(123);
        let sig = sch.sign(&key, &id, &msg, &mut rng);
        let f = vec![Scalar::from(2)];
        let doubled = msg.scale(&f[0]);
        let doubled_sig = sch.combine(&f, &vec![sig]);
        let ver = sch.verify(&key.vk, &id, &doubled, &doubled_sig);
        assert!(ver);
    }
    #[test]
    fn catalano12_sign_combine_verify() {
        let mut rng = rand_chacha::ChaChaRng::from_entropy();
        let msg_length = 5;
        let sch = Cat12::setup(msg_length, 1);
        let key = sch.keygen(&mut rng);

        let msg = vec![Scalar::random(&mut rng); msg_length];
        let msg = [vec![Scalar::from(1), Scalar::zero()], msg].concat();
        let id = Scalar::from(123);
        let sig = sch.sign(&key, &id, &msg, &mut rng);
        let msg2 = vec![Scalar::random(&mut rng); msg_length];
        let msg2 = [vec![Scalar::zero(), Scalar::from(1)], msg2].concat();
        let sig2 = sch.sign(&key, &id, &msg2, &mut rng);
        let f = vec![Scalar::from(1), Scalar::from(1)];
        let added = msg.add_vec(&msg2);
        let added_sig = sch.combine(&f, &vec![sig, sig2]);
        let ver = sch.verify(&key.vk, &id, &added, &added_sig);
        assert!(ver);
    }
}
