use crate::ff::Field;
use crate::group::GroupEncoding;
use crate::misc::bls_util::pair_proj;
use crate::misc::bls_util::HashesToG1;
use crate::misc::types::File;
use bls12_381::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Scalar};
use group::Group;
use rand::RngCore;
use rand_seeder::Seeder;
use rand_seeder::SipRng;

pub struct NCZha2018 {
    msg_length: usize,
    pub file_length: usize,
    h: G2Affine,
    z: Scalar,
    hz: G2Projective,
}
impl NCZha2018 {
    pub fn setup(msg_length: usize, file_length: usize, rng: &mut impl RngCore) -> Self {
        let h = G2Affine::generator();
        let z = Scalar::random(&mut *rng);
        let hz = h * z;
        Self {
            msg_length,
            file_length,
            h,
            z,
            hz,
        }
    }
    pub fn extract(&self, id: &ID, rng: &mut impl RngCore) -> SK {
        let r = Scalar::random(&mut *rng);
        let hr = self.h * r;
        let hash_input = [hr.to_bytes().as_ref(), id].concat();
        let y = r + (self.z * Self::h0(&hash_input));
        let gs: Vec<G1Projective> = (0..self.msg_length)
            .map(|_| G1Projective::random(&mut *rng))
            .collect();
        SK { y, hr, gs }
    }
    pub fn sign(&self, sk: &SK, gam: &[u8], msg: &Vector) -> Sig {
        let mut sig = G1Projective::identity();

        for (i, unit) in msg[self.msg_length..].iter().enumerate() {
            if unit != &Scalar::zero() {
                sig += NCZha2018::h1(gam, &i.to_be_bytes()) * unit;
            }
        }
        for (gj, msg_j) in sk.gs.iter().zip(&msg[..self.msg_length]) {
            sig += gj * msg_j;
        }

        sig * sk.y
    }
    pub fn combine(&self, f: &Coefs, sigs: &Vec<Sig>) -> Sig {
        let mut res = Sig::identity();
        for (sig, fi) in sigs.iter().zip(f) {
            // println!("multiplying signature by {:?}", fi);
            res += sig * fi;
        }
        res
    }
    pub fn verify(
        &self,
        id: &ID,
        hr: &G2Projective,
        gam: &[u8],
        msg: &Vector,
        sig: &Sig,
        gs: &Vec<G1Projective>,
    ) -> bool {
        let p1 = pairing(&G1Affine::from(sig), &self.h);

        let mut rec_sig = G1Projective::identity();
        for (i, unit) in msg[self.msg_length..].iter().enumerate() {
            if unit != &Scalar::zero() {
                rec_sig += NCZha2018::h1(gam, &i.to_be_bytes()) * unit;
            }
        }
        for (gj, msg_j) in gs.iter().zip(&msg[..self.msg_length]) {
            rec_sig += gj * msg_j;
        }

        let hash_input = [hr.to_bytes().as_ref(), id].concat();
        let inp2 = hr + (self.hz * Self::h0(&hash_input));
        let p2 = pair_proj(&rec_sig, &inp2);
        p1 == p2
    }
    fn h0(inp: &[u8]) -> Scalar {
        let mut rng: SipRng = Seeder::from(inp).make_rng();
        Scalar::random(&mut rng)
    }
    fn h1(in1: &[u8], in2: &[u8]) -> G1Projective {
        let inp = [in1, in2].concat();
        (&inp[..]).hash_g1()
    }
}

impl NCZha2018 {
    pub fn sign_file_single_key(&self, sk: &SK, file_id: &[u8], file: &File<Scalar>) -> Vec<Sig> {
        file.iter()
            .map(|packet| self.sign(sk, file_id, packet))
            .collect()
    }
    pub fn sign_file_multiple_keys(
        &self,
        sks: &Vec<SK>,
        file_id: &[u8],
        file: &File<Scalar>,
    ) -> Vec<Sig> {
        sks.iter()
            .zip(file)
            .map(|(sk, msg)| self.sign(sk, file_id, msg))
            .collect()
    }

    pub fn verify_file_single_vk(
        &self,
        signer_id: &ID,
        file_id: &[u8],
        key_hr: &G2Projective,
        key_gs: &Vec<G1Projective>,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(sigs)
            .map(|(packet, sig)| self.verify(signer_id, key_hr, file_id, packet, sig, key_gs))
            .collect()
    }

    pub fn verify_file_multiple_vks(
        &self,
        signer_ids: &Vec<ID>,
        file_id: &[u8],
        key_hrs: &Vec<G2Projective>,
        key_gs: &Vec<Vec<G1Projective>>,
        file: &File<Scalar>,
        sigs: &Vec<Sig>,
    ) -> Vec<bool> {
        file.iter()
            .zip(signer_ids)
            .zip(key_hrs)
            .zip(key_gs)
            .zip(sigs)
            .map(|((((packet, signer_id), key_hr), key_g), sig)| {
                self.verify(signer_id, key_hr, file_id, packet, sig, key_g)
            })
            .collect()
    }
}

pub type ID = Vec<u8>;
#[derive(Debug, Clone)]
pub struct SK {
    pub y: Scalar,
    pub hr: G2Projective,
    pub gs: Vec<G1Projective>,
}
pub type Msg = Scalar;
pub type Vector = Vec<Msg>;
pub type Sig = G1Projective;
pub type Coefs = Vec<Scalar>;

#[cfg(test)]
mod tests {
    use rand::{thread_rng, SeedableRng};
    use rand_chacha::ChaChaRng;

    use super::*;

    #[test]
    fn zha2018_sign_verify() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = NCZha2018::setup(1, 1, &mut rng);
        let id1: ID = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let key = sch.extract(&id1, &mut rng);
        let msg = vec![Scalar::from(16), Scalar::from(1)];

        let filename = b"filename";
        let sig = sch.sign(&key, &filename[..], &msg);
        let ver = sch.verify(&id1, &key.hr, &filename[..], &msg, &sig, &key.gs);
        assert!(ver)
    }

    #[test]
    fn zha2018_sign_verify_longer_message() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = NCZha2018::setup(8, 1, &mut rng);
        let id1: ID = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let key = sch.extract(&id1, &mut rng);
        let msg = vec![
            Scalar::from(0),
            Scalar::from(2),
            Scalar::from(4),
            Scalar::from(6),
            Scalar::from(8),
            Scalar::from(10),
            Scalar::from(12),
            Scalar::from(14),
            Scalar::from(1),
        ];
        let filename = b"signing longer messages";
        let sig = sch.sign(&key, &filename[..], &msg);
        let ver = sch.verify(&id1, &key.hr, &filename[..], &msg, &sig, &key.gs);
        assert_eq!(ver, true);
    }
    #[test]
    fn zha2018_sign_double_verify() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = NCZha2018::setup(1, 1, &mut rng);
        let id1: ID = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let key = sch.extract(&id1, &mut rng);
        let msg = vec![Scalar::from(16), Scalar::from(1)];

        let filename = b"doubling";
        let sig = sch.sign(&key, &filename[..], &msg);
        let doubled = sch.combine(&vec![Scalar::from(2)], &vec![sig]);
        let doubled_msg = vec![Scalar::from(32), Scalar::from(2)];
        let ver = sch.verify(
            &id1,
            &key.hr,
            &filename[..],
            &doubled_msg,
            &doubled,
            &key.gs,
        );
        assert_eq!(ver, true);
    }
    #[test]
    fn zha2018_combine_longer_message() {
        let mut rng = ChaChaRng::from_entropy();
        let sch = NCZha2018::setup(8, 2, &mut rng);
        let id1: ID = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let key = sch.extract(&id1, &mut rng);
        let msg = vec![
            Scalar::from(0),
            Scalar::from(2),
            Scalar::from(4),
            Scalar::from(6),
            Scalar::from(8),
            Scalar::from(10),
            Scalar::from(12),
            Scalar::from(14),
            Scalar::from(1),
            Scalar::from(0),
        ];
        let filename = b"signing longer messages";
        let sig = sch.sign(&key, &filename[..], &msg);

        let msg2 = vec![
            Scalar::from(10),
            Scalar::from(12),
            Scalar::from(14),
            Scalar::from(16),
            Scalar::from(18),
            Scalar::from(110),
            Scalar::from(112),
            Scalar::from(114),
            Scalar::from(0),
            Scalar::from(1),
        ];
        let sig2 = sch.sign(&key, &filename[..], &msg2);

        let com = sch.combine(&vec![Scalar::from(2), Scalar::from(3)], &vec![sig, sig2]);

        let msg_com = vec![
            Scalar::from(2 * 0) + Scalar::from(3 * 10),
            Scalar::from(2 * 2) + Scalar::from(3 * 12),
            Scalar::from(2 * 4) + Scalar::from(3 * 14),
            Scalar::from(2 * 6) + Scalar::from(3 * 16),
            Scalar::from(2 * 8) + Scalar::from(3 * 18),
            Scalar::from(2 * 10) + Scalar::from(3 * 110),
            Scalar::from(2 * 12) + Scalar::from(3 * 112),
            Scalar::from(2 * 14) + Scalar::from(3 * 114),
            Scalar::from(2),
            Scalar::from(3),
        ];

        let ver = sch.verify(&id1, &key.hr, &filename[..], &msg_com, &com, &key.gs);
        assert_eq!(ver, true);
    }

    #[test]
    fn zha2018_stepwise() {
        let _n = 1;
        let mut rng = thread_rng();
        let h = G2Affine::generator();
        let z = Scalar::random(&mut rng);
        let hz = h * z;

        // extract
        let id = [1u8, 2u8, 3u8];

        let r = Scalar::random(&mut rng);
        let hr = h * r;
        let to_hash = [hr.to_bytes().as_ref(), &id].concat();
        let y = r + (z * NCZha2018::h0(&to_hash));

        // sign
        let filename = b"filename";
        let msg = vec![Scalar::from(16)];

        let sig = NCZha2018::h1(&filename[..], &[0]) * msg[0];
        let sig = sig * y;

        // verify
        let p1 = pairing(&G1Affine::from(sig), &h);

        let p2in1 = NCZha2018::h1(&filename[..], &[0]) * msg[0];
        let p2in2 = hr + (hz * NCZha2018::h0(&to_hash));
        let p2 = pair_proj(&p2in1, &p2in2);

        assert_eq!(p1 == p2, true);
    }
}
