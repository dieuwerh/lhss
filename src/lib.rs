extern crate bls12_381 as bls;
extern crate ed25519_dalek as ed;
extern crate ff;
extern crate group;
extern crate hmac;
extern crate num_bigint_dig as num_bigint;
extern crate rand;
extern crate rand_chacha;

pub mod misc;
pub mod schemes;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
